﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Tags;

namespace LumUlt
{

    public static class Files
    {

        public static tblFiles GetBasicInfo(string path)
        {
            tblFiles file = new tblFiles();
            FileInfo info = new FileInfo(path);
            file.Url = path;
            file.Folder = info.Directory.ToString();
            file.Size = Math.Round(Convert.ToDouble(info.Length) / 1024 / 1024, 2);
            file.Filename = Explorer.GetFileName(path);
            file.DateCreated = info.CreationTime;
            return file;
        }

        public static tblFiles GetFullInfo(string path,int strm=0)
        {//optional strm (stream) da se ne zove BASS_StreamCreateFile vise puta ako je stream vec postoji. Npr kod Player.Play

            tblFiles file = GetBasicInfo(path);
            if(strm==0) strm = Bass.BASS_StreamCreateFile(path, 0, 0, BASSFlag.BASS_DEFAULT);
            long len = Bass.BASS_ChannelGetLength(strm, BASSMode.BASS_POS_BYTES);
            file.IntDuration = Convert.ToInt32(Bass.BASS_ChannelBytes2Seconds(strm, len));
            file.Duration = GetDurString(Convert.ToInt16 (file.IntDuration));
            Un4seen.Bass.AddOn.Tags.TAG_INFO tagInfo=new Un4seen.Bass.AddOn.Tags.TAG_INFO();
            Un4seen.Bass.AddOn.Tags.BassTags.BASS_TAG_GetFromFile(strm, tagInfo);
            file.Author = tagInfo.artist;
            file.Title = tagInfo.title;
            file.Album = tagInfo.album;
            file.Bitrate = tagInfo.bitrate;
            file.Comment = tagInfo.comment;
            file.Year = tagInfo.year;
            file.Genre = tagInfo.genre;

            return file;
        }


        public static string GetDurString(int sec)
        {
            string DurationString = "00:00";
            int CurrentDuration = sec;
            int DurationHour = CurrentDuration / 3600;
            int DurationMin = 0;
            int DurationSec = 0;

            if (DurationHour >= 1)
            {
                DurationMin = ((CurrentDuration % 3600) / 60);
                DurationSec = ((CurrentDuration % 3600) % 60);
                DurationString = DurationHour.ToString("00") + ":" + DurationMin.ToString("00") + ":" + DurationSec.ToString("00");
            }
            else
            {
                DurationMin = CurrentDuration / 60;
                DurationSec = CurrentDuration % 60;
                DurationString =  DurationMin.ToString("00") + ":" + DurationSec.ToString("00");
            }
            return DurationString;
        }


    }

}
