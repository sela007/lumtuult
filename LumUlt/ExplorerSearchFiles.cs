﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LumUlt
{
    public static partial class Explorer
    {
        public delegate void SearchFilesEventsDelegate(object sender, SearchFilesEventArgs e);
        public static event SearchFilesEventsDelegate FileFound;
        public static event SearchFilesEventsDelegate CurrentSearchFolderChanged;
        public static event SearchFilesEventsDelegate SearchCompleted;

        public static bool MatchWithFolder { get; set; }
        public static string Filter { get; set; }
        public static List<string> Folders { get; set; }
        public static List<string> Extensions { get; set; }
        public static int Count { get; set; }

        private static BackgroundWorker SearchWorker;
        private static void ReloadSearchWorker()
        {
            if (SearchWorker != null)
            {
                SearchWorker.DoWork -= SearchWorker_DoWork;
                SearchWorker.RunWorkerCompleted -= SearchWorker_RunWorkerCompleted;
                SearchWorker.ProgressChanged -= SearchWorker_ProgressChanged;

            }

            SearchWorker = new BackgroundWorker();
            SearchWorker.WorkerReportsProgress = true;
            SearchWorker.WorkerSupportsCancellation = true;
            SearchWorker.DoWork += SearchWorker_DoWork;
            SearchWorker.RunWorkerCompleted += SearchWorker_RunWorkerCompleted;
            SearchWorker.ProgressChanged += SearchWorker_ProgressChanged;
        }

        static void SearchWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            SearchFilesEventArgs args = new SearchFilesEventArgs { FilePath = e.UserState.ToString(), FilesFound = Count };
                   
            if (FileFound != null)
                FileFound(null, args);

        }

        static void SearchWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (SearchCompleted != null) SearchCompleted(null, new SearchFilesEventArgs { FilesFound = Count });
     
        }

        static void SearchWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            LoopFolders(SearchPath, SearchString);

            //ako je u vrijeme dok se traže fajlovi pokrenuto novo traženje onda očisti memoriju i izađi
            if (worker.CancellationPending)
            {
                e.Cancel = true;
                //ako smo došli do ovdje znaći da SearchWorker više 
                //nije worker (sender) i više se neče koristiti
                worker.Dispose();
                return;
            }

        }
        private static string SearchString { get; set; }
        private static string SearchPath { get; set; }
        public static void Search(string strSearch, string path)
        {
            Table.Rows.Clear();
            Count = 0;
            SearchString = strSearch;
            SearchPath = path;
            if (SearchWorker.IsBusy)
            {
                //postavljanje bw.CancellationPending prop = true
                SearchWorker.CancelAsync();
                //pošto je još u tijeku  traženje potrebno je napraviti novi bw objekt da bi
                //se odma počelo sa novim traženjem 
                ReloadSearchWorker();
            }
            SearchWorker.RunWorkerAsync();

        }
        

        public static void LoopFolders(string fPath, string fItem)
        {
            try
            {
                if (CurrentSearchFolderChanged != null)
                {
                    SearchFilesEventArgs args = new SearchFilesEventArgs { FilePath = fPath,FilesFound =Count };
                    CurrentSearchFolderChanged(null,args);
                }

                SearchFolder(fPath, fItem);
                string[] sFolders = Directory.GetDirectories(fPath, "*", SearchOption.TopDirectoryOnly);
                foreach (string sFolder in sFolders)
                {
                    if (CurrentSearchFolderChanged != null)
                    {
                        SearchFilesEventArgs args = new SearchFilesEventArgs { FilePath = fPath, FilesFound = Count };
                        CurrentSearchFolderChanged(null, args);
                    }
                    LoopFolders(sFolder, fItem);
                }
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }

        }

  
        public static void SearchFolder(string fPath, string fItem)
        {
            try
            {
                string[] sFiles = null;
                 
                foreach (string ext in Extensions)
                {
                    if (!MatchWithFolder)
                    {
                        sFiles = Directory.GetFiles(fPath, "*" + fItem + "*" + ext, SearchOption.TopDirectoryOnly);
                    }
                    else
                    {
                        if (fPath.ToLower().Contains(fItem.ToLower()))  
         
                        {
                            sFiles = Directory.GetFiles(fPath, "*" + ext, SearchOption.TopDirectoryOnly);
                        }
                        else
                        {
                            sFiles = Directory.GetFiles(fPath, "*" + fItem + "*" + ext, SearchOption.TopDirectoryOnly);
                        }
                    }

                }
                foreach (string f in sFiles)
                {
                    
                    AddFile(f);
                    Count += 1;
                    SearchWorker.ReportProgress(Count, f);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            

        }

    }

    public class SearchFilesEventArgs : EventArgs
    {
        public string FilePath { get; set; }
        public int FilesFound { get; set; }
    }
}
