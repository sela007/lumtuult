﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace LumUlt
{
    public partial class frmFileInfo : Form
    {
        public frmFileInfo(tblFiles file)
        {
            InitializeComponent();
            //foreach (var p in file.GetType().GetProperties().Where(p => p.GetGetMethod().GetParameters().Count() == 0))
            //{
            //    txtProperties.Text += p.GetValue(file, null);
            //}
        }

        public frmFileInfo()
        {
            InitializeComponent();
      
        }

        private int selIndex = -1;
        public void LoadFile(string path)
        {
            this.Text = Explorer.GetFileName(path);
            dgProp.Rows.Clear();
            tblFiles file = LumUlt.Database.GetFile(path);
            if(file==null)
            file= Files.GetFullInfo(path);

            foreach (var p in file.GetType().GetProperties().Where(p => p.GetGetMethod().GetParameters().Count() == 0))
            {
                dgProp.Rows.Add(p.Name, p.GetValue(file, null));
            }
            //if (selIndex != -1) lstProp.SetSelected(selIndex, true);


            this.Visible = true;
        }

        private void frmFileInfo_Load(object sender, EventArgs e)
        {

        }

    

        private void frmFileInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Visible = false;
            } 
        }
    }
}
