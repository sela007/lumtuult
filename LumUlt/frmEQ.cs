﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LumUlt
{
    public partial class frmEQ : Form
    {
        public frmEQ()
        {
            InitializeComponent();
             
        }
         
        private void frmEQ_Load(object sender, EventArgs e)
        {

        }
 
        private void EQScroll(object sender, EventArgs e)
        {
            TrackBar EQ=(TrackBar)sender;
            int intBand = Convert.ToInt32(EQ.Tag);
            Player.EQBandValue[intBand] = EQ.Value;
            Player.UpdateEQ(intBand);
        }

        private void EQBand_Scroll(object sender, EventArgs e)
        {

        }
    }
}
