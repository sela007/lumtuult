﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace LumUlt
{
    public static partial class Explorer
    {
        static Explorer()
        {
            Table = new DataTable();
            Table.Columns.Add("FileName", typeof(string));
            Table.Columns.Add("DateCreated", typeof(DateTime));
            Table.Columns.Add("Size", typeof(double));
            Table.Columns.Add("Url", typeof(string));
            Extensions = new List<string>();
            Extensions.Add(".mp3");
            MatchWithFolder = false;
            ReloadSearchWorker();
            NodeFavorites = new TreeNode("Favorites", 2, 2);
            NodeFavorites.Tag = "favorites";

        }


       
        public static void UpdateDrives(TreeNode node)
        {
            List<DriveInfo> drives = System.IO.DriveInfo.GetDrives().ToList();
            foreach (DriveInfo info in DriveInfo.GetDrives())
            {
                if (info.IsReady)
                {
                    bool dExist = false;
                    foreach (TreeNode n in node.Nodes)
                    {
                        if (n.Tag.ToString() == info.RootDirectory.FullName) dExist = true;
                    }
                    if (dExist == false)
                    {
                        AddDrive(node,info);
                    }
                }
            }
        }

        public static void RemoveNotReadyDrives(TreeNode node)
        {
             
                    foreach (TreeNode n in node.Nodes)
                    {
                        if (n != null)
                        {
                            if (!IsSpecialFolder(n.Tag.ToString()))
                            {
                                if (System.IO.Directory.Exists(n.Tag.ToString()) == false)
                                {
                                    node.Nodes.Remove(n);
                                }
                            }
                        }
                    }
       
        }

        public static DataTable Table {get;set;}

 

        public static void AddDrive(TreeNode node, DriveInfo info)
        {
                    TreeNode n = GetNode(info.RootDirectory.FullName);
                    AddSubFolders(n);
                    node.Nodes.Add(n);
        }

        
      

        public static void AddSubFolders(TreeNode node,bool expand=false )
        {
            if (node.Nodes.Count == 0)
            {
                    foreach (var dir in System.IO.Directory.GetDirectories(node.Tag.ToString()))
                    {
                        DirectoryInfo info = new DirectoryInfo(dir);
                        if ((info.Attributes & FileAttributes.System) != FileAttributes.System & (info.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                        {
                            TreeNode n = GetNode(dir);
                            node.Nodes.Add(n);
                        }
                    }
                    if (expand) node.Expand();
            }
        }

 

        public static TreeNode GetNode(string path)
        {
            string nodeText = GetFileName(path).ToUpper();
            if (nodeText == "") nodeText = path;
            TreeNode node = new TreeNode(nodeText);

            switch (path.ToLower())
            {
                case "mycomputer":
                    node.Tag = "mycomputer";
                    node.Text = "My computer";
                    node.SelectedImageIndex = 2;
                    node.ImageIndex = 2;
                    foreach (DriveInfo info in DriveInfo.GetDrives())
                    {
                        if (info.IsReady)
                        {
                            AddDrive(node, info);
                        }
                    }
                    break;
                case "database":
                    node.Tag = "database";
                    node.SelectedImageIndex = 2;
                    node.ImageIndex = 2;
                break;
                case "radios":
                    node.Tag = "radios";
                    node.SelectedImageIndex = 4;
                    node.ImageIndex = 4;
                break;
                default:
                    node.Tag = path;
                break;
            }

          

            if (!IsSpecialFolder(path))
            {
                if (HasSubfolders(path))
                {
                    node.ImageIndex = 1;
                    node.SelectedImageIndex = 1;
                    //node.Nodes.Add("*");
                }            
                if (path.Length == 3)
                {
                    node.Tag = path;
                    node.SelectedImageIndex = 3;
                    node.ImageIndex = 3;
                }
            }


            return node;
        }

        //private static int GetNodeImageIndex(TreeNode node, string path)
        //{
        //    switch (path.ToLower())
        //    {
        //        case "database":
        //            return 2;

        //        case "folder":
        //            return 0;

        //        default:
        //            return 0;
        //    }
        //}

        public static string[] SelectedFiles;

        public static void GetSelectedFiles(DataGridView dg)
        {
            SelectedFiles = new string[dg.SelectedRows.Count];

            if (dg.SelectedRows != null)
            {
                int i = -1;
                foreach (DataGridViewRow dr in dg.Rows)
                {
                    if (dr.Selected)
                    {
                        i++;
                        Explorer.SelectedFiles[i] = dr.Cells["url"].Value.ToString();
                    }
                }

            }
        }


        public static string GetFileName(string path)
        {
            return System.IO.Path.GetFileName(path);
        }
        public static string GetFileNameWEXT(string path)
        {
            return System.IO.Path.GetFileNameWithoutExtension(path);
        }

        public static bool HasSubfolders(string path)
        {
            IEnumerable<string> subfolders = Directory.EnumerateDirectories(path);
            return subfolders != null && subfolders.Any();
        }


        public static DataTable GetFilesFromFolder(string path)
        {

            Table.Rows.Clear();
            if (IsSpecialFolder(path )) return Table;

            string[] files = Directory.GetFiles(path, "*.mp3", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
                AddFile(file);

            Location = ExplorerLocation.Folder;
            return Table;
        }

        private static void AddFile(string file)
        {
                tblFiles fileInfo = Files.GetBasicInfo(file);
                Table.Rows.Add(fileInfo.Filename, fileInfo.DateCreated, fileInfo.Size,fileInfo.Url);
        }

        public static bool IsSpecialFolder(string path){

            switch (path.ToLower())
            {
                case "mycomputer": return true;
                case "database": return true;
                case "favorites": return true;
                case "playlist": return true;
                case "tags": return true;
                case "radios": return true;
                default: return false;
            }
          
        }

        
        static void SearchFiles_SearchCompleted(object sender, SearchFilesEventArgs e)
        {
            
        }

        static void SearchFiles_CurrentFolderChanged(object sender, SearchFilesEventArgs e)
        {
            
        }

        static void SearchFiles_FileFound(object sender, SearchFilesEventArgs e)
        {
            AddFile(e.FilePath);
        }



        public static ExplorerLocation Location { get; set; }
        public enum ExplorerLocation {Nothing, Database, Folder, Playlist, Radios }

        public static TreeNode NodeFavorites { get; set; }



    }


    #region SHFILEOPERATION
    class ShellFileOperation
    {
        [DllImport("shell32.dll", SetLastError = true,CharSet=CharSet.Auto)]
        protected static extern int SHFileOperation(ref SHFileOpStruct lpFileOp);
        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        protected static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);

        public enum FileOp
        {
            Move = 0x0001,
            Copy = 0x0002,
            Delete = 0x0003,
            Rename = 0x0004
        }

        [Flags]
        public enum FileOpFlags
        {
            MultiDestFiles = 0x0001,
            ConfirmMouse = 0x0002,
            Silent = 0x0004,
            RenameCollision = 0x0008,
            NoConfirmation = 0x0010,
            WantMappingHandle = 0x0020,
            AllowUndo = 0x0040,
            FilesOnly = 0x0080,
            SimpleProgress = 0x0100,
            NoConfirmMkDir = 0x0200,
            NoErrorUI = 0x0400,
            NoCopySecurityAttributes = 0x0800,
            NoRecursion = 0x1000,
            NoConnectedElements = 0x2000,
            WantNukeWarning = 0x4000,
            NoRecursiveReparse = 0x8000
        }

        [StructLayout(LayoutKind.Sequential)]
        protected struct SHFileOpStruct
        {
            public IntPtr hwnd;
            public uint wFunc;
            public string pFrom;
            public string pTo;
            public FileOpFlags fFlags;
            public bool anyOperationsAborted;
            public IntPtr nameMapping;
            public string progressTitle;
        }

        protected static SHFileOpStruct fos;

        public static void DoFileOperation(string[] src, string destinationFolder, string dialogTitle,
           FileOp op, Form parent)
        {
            FileOpFlags flags = ShellFileOperation.FileOpFlags.MultiDestFiles & ShellFileOperation.FileOpFlags.SimpleProgress;
            string fs = flags.ToString("G");
            if (fs.IndexOf("WantMappingHandle") > -1)
                throw new ArgumentException("Mapping handle argument is not specified in this method");
             
            DoFileOperation(src, destinationFolder, dialogTitle, op, flags, parent, IntPtr.Zero);
        }

        private static void DoFileOperation(string[] src, string dest, string dialogTitle,
           FileOp op, FileOpFlags flags, Form parent, IntPtr mappingHandle)
        {

            fos = new SHFileOpStruct();
            fos.hwnd = parent.Handle;
            foreach (string file in src)
            {
                fos.pFrom += file + '\0';

                string fName = System.IO.Path.GetFileName(file);
                //string fFolder = System.IO.Path.GetDirectoryName(file);
                fos.pTo += System.IO.Path.Combine(dest, fName) + '\0';
            }
            fos.pFrom +='\0';
            fos.pTo += '\0';

    
            fos.pTo = dest;
            fos.progressTitle = dialogTitle;
            fos.wFunc = (uint)op;
            fos.fFlags = flags;
            fos.anyOperationsAborted = false;
            fos.nameMapping = mappingHandle;

            SHFileOperation(ref fos);
        }

        //FILE PROPERTIES

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpVerb;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpFile;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpParameters;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpDirectory;
            public int nShow;
            public IntPtr hInstApp;
            public IntPtr lpIDList;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpClass;
            public IntPtr hkeyClass;
            public uint dwHotKey;
            public IntPtr hIcon;
            public IntPtr hProcess;
        }

        private const int SW_SHOW = 5;
        private const uint SEE_MASK_INVOKEIDLIST = 12;
        public static bool ShowFileProperties(string Filename)
        {
            SHELLEXECUTEINFO info = new SHELLEXECUTEINFO();
            info.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(info);
            info.lpVerb = "properties";
            info.lpFile = Filename;
            info.nShow = SW_SHOW;
            info.fMask = SEE_MASK_INVOKEIDLIST;
            return ShellExecuteEx(ref info);
        }

    } 
    #endregion

}
