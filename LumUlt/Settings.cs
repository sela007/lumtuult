﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace LumUlt
{
    public class Settings
    {
        public List<EQPreset> EQPresets;
        public int Volume { get; set; }
        public List<NodeSettings> nodeSettings { get; set; }
        public ColumnsSettings DatabaseColumns { get; set; }
        public ColumnsSettings FilesColumns { get; set; }
        public ColumnsSettings RadiosColumns { get; set; }

        public Color TagSelectedBackColor { get; set; }
        public Color TagSelectedForeColor { get; set; }
        public Color TagBackColor { get; set; }
        public Color TagForeColor { get; set; }


        public Settings()
        {
            EQPresets = new List<EQPreset>();
            Volume = 61;
            TagSelectedBackColor = Color.DarkGray;
            TagSelectedForeColor = Color.WhiteSmoke;
            TagBackColor = Color.Transparent;
            TagForeColor = Color.DarkGray;
           
        }
        public void Save()
        {
            //settings sett = new settings();
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Settings));

            System.IO.StreamWriter file = new System.IO.StreamWriter(
                @"c:\settings.xml");
            writer.Serialize(file, this);
            file.Close();
        }

        public static Settings Load(string fPath)
        {
            if(System.IO.File.Exists (fPath) ){
                //settings sett = new settings();
                System.Xml.Serialization.XmlSerializer reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(Settings));
                using (System.IO.StreamReader file = new System.IO.StreamReader(
                    fPath))
                {
                    return (Settings)reader.Deserialize(file);
                }
            }
            else
            {return new Settings() ;}
        }


    }


    public class ColumnsSettings
    {
        public ColumnsSettings() { Columns = new List<ColumnSettings>(); }
        public int SortColumn { get; set; }
        public int SortDirection { get; set; }
        public List<ColumnSettings> Columns;
        public static ColumnsSettings GetSettings(DataGridView dg)
        {
            ColumnsSettings newSettings = new ColumnsSettings();


            if (dg.SortedColumn == null) { newSettings.SortColumn = 0; }
            else
            {newSettings.SortColumn = dg.SortedColumn.Index;}
            newSettings.SortDirection = 0;

            if (dg.SortOrder == SortOrder.Descending )
                newSettings.SortDirection = 1;
            

            for (int i = 0; i <= dg.Columns.Count - 1; i++)
            {
                ColumnSettings cs = new ColumnSettings();
                cs.DisplayIndex = dg.Columns[i].DisplayIndex;
                cs.Visible = dg.Columns[i].Visible;
                cs.Width = dg.Columns[i].Width;
                newSettings.Columns.Add(cs);
            }
            return newSettings;
        }

        public static void ApplySettings(DataGridView dg, ColumnsSettings cs)
        {
            for (int i = 0; i <= dg.Columns.Count - 1; i++)
            {
                dg.Columns[i].Width = cs.Columns[i].Width;
                dg.Columns[i].DisplayIndex = cs.Columns[i].DisplayIndex;
                dg.Columns[i].Visible = cs.Columns[i].Visible;
            }

            if (cs.SortDirection == 0)
            {
                dg.Sort(dg.Columns[cs.SortColumn],System.ComponentModel.ListSortDirection.Ascending );
            }
            else
            {
                dg.Sort(dg.Columns[cs.SortColumn], System.ComponentModel.ListSortDirection.Descending );
            }
        }
    }

    public class ColumnSettings
    {
        public ColumnSettings() { }
        public int Width { get; set; }
        public int DisplayIndex { get; set; }
        public bool Visible { get; set; }

    }

    public class NodeSettings
    {
        public NodeSettings() { Nodes = new List<NodeSettings>(); }
        public string Text { get; set; }
        public string Tag { get; set; }
        public int ImageIndex { get; set; }
        public List<NodeSettings> Nodes;
        public bool IsExpanded { get; set; }

        public static NodeSettings GetNodeSettings(TreeNode source)
        {
            NodeSettings returnNode = new NodeSettings();
            returnNode.Text = source.Text;
            returnNode.Tag = source.Tag.ToString();
            returnNode.IsExpanded = source.IsExpanded;
            returnNode.ImageIndex = source.ImageIndex;
            foreach (TreeNode n in source.Nodes)
            {
               
                NodeSettings ns = new NodeSettings();
                ns.Text = n.Text;
                ns.Tag = n.Tag.ToString();
                ns.ImageIndex = n.ImageIndex;
                ns.IsExpanded = n.IsExpanded;
                ns.Nodes = GetNodesSettings(n);
                returnNode.Nodes.Add(ns);
            }
            return returnNode;
        }

        private static List<NodeSettings> GetNodesSettings(TreeNode source)
        {   //Rekurzivna
            List<NodeSettings> returnNode = new List<NodeSettings>();
            foreach (TreeNode n in source.Nodes)
            {
                NodeSettings ns = new NodeSettings();
                ns.Text = n.Text;
                ns.Tag = n.Tag.ToString();
                ns.ImageIndex = n.ImageIndex;
                ns.IsExpanded = n.IsExpanded;
                ns.Nodes = GetNodesSettings(n);
                returnNode.Add(ns);
            }
            return returnNode;
        }


        public TreeNode GetNode()
        {
            TreeNode returnNode = new TreeNode(Text,ImageIndex, ImageIndex);
            returnNode.Tag = Tag;

            foreach (NodeSettings ns in Nodes)
            {
                TreeNode node2 = new TreeNode(ns.Text, ns.ImageIndex, ns.ImageIndex);
                node2.Tag = ns.Tag;
                node2.Nodes.AddRange(GetNodes(ns));
                returnNode.Nodes.Add(node2);
                if (ns.IsExpanded) node2.Expand();
            }

            if (IsExpanded) returnNode.Expand();
            return returnNode;
        }


        private TreeNode[] GetNodes(NodeSettings source)
        {   //rekurziva
            List<TreeNode> returnNodes = new List<TreeNode>();
            foreach (NodeSettings ns in source.Nodes)
            {
                TreeNode node2 = new TreeNode(ns.Text, ns.ImageIndex, ns.ImageIndex);
                node2.Tag = ns.Tag;
                node2.Nodes.AddRange(GetNodes(ns));
                returnNodes.Add(node2);
                if (ns.IsExpanded) node2.Expand();
            }
            return returnNodes.ToArray();
        }


    }




}
