﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace LumUlt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        TreeNode MyComputerNode;
        Settings settings;

        private void Form1_Load(object sender, EventArgs e)
        {
            FileInfoForm = new frmFileInfo();

            tv0.Nodes.Add("Database", "Database", 0);
            tv0.Nodes.Add("Favorites", "Favorites", 0);
            tv0.Nodes.Add("Radios", "Radios", 0);
            tv0.Nodes.Add("MyComputer", "mycomputer", 0);

            List < tblTags > tags = Database.GetTags();
            TagStrip.Items.Clear();
            foreach (tblTags tag in tags)
            {
                ToolStripButton itm = new ToolStripButton(tag.Tag, null, PlayerTagClick );
                 
                TagStrip.Items.Add(itm);
            }

            
            settings = Settings.Load(@"c:\settings.xml");

            lstEQPresets.Items.Clear();
            foreach (EQPreset p in settings.EQPresets) lstEQPresets.Items.Add(p.Name);
            trVol.Value = settings.Volume;
            Player.Volume = settings.Volume;

            Player.FileChanged += SongChanged;


            settings.nodeSettings = null;
            if (settings.nodeSettings != null)
            {
                foreach (NodeSettings ns in settings.nodeSettings)
                {
                    tv1.Nodes.Add(ns.GetNode());

                }
            }
            else
            {
                tv1.Nodes.Add(Explorer.GetNode("Database"));
                tv1.Nodes.Add(Explorer.GetNode("Radios"));
                MyComputerNode = Explorer.GetNode("mycomputer");
                tv1.Nodes.Add(MyComputerNode);
                tv1.Nodes.Add(Explorer.NodeFavorites);
            }

            Player.SpectrumMode = Player.SpectrumType.Wave;
            Player.SpectrumColor1 = Color.DarkGray;
            Player.SpectrumColor2 = Color.Gray;
            Player.SpectrumColor3 = Color.Green;
            //dgFiles.DataSource = Explorer.tblFiles;
            //lmdbEnt en = new lmdbEnt();
            //var tbl = from filess in en.tblFiles select filess;
            //
            //BindingSource bs = new BindingSource();
            //bs.DataSource = en.tblFiles;
            //dgFiles.DataSource = bs.DataSource;
            
            dgFiles.DataSource = Explorer.Table;
            Player.StateChanged+=Player_StateChanged;
            Player.StreamInfoCompleted += Player_StreamInfoCompleted;
            RadioManager.SearchCompleted += RadioManager_SearchCompleted;
            Explorer.CurrentSearchFolderChanged += Explorer_CurrentSearchFolderChanged;
            Explorer.FileFound += Explorer_FileFound;
            Explorer.SearchCompleted += Explorer_SearchCompleted;
        }

        void PlayerTagClick(object sender, EventArgs e)
        {
             
            var itm=(ToolStripButton )sender ;
             
            itm.BackColor = settings.TagSelectedBackColor;
            itm.ForeColor = settings.TagSelectedForeColor;
            TagStrip.Refresh();
            if (itm.Selected == true)
            {
                itm.Select();
            }
            else
            {
                 
            }
        }

        void Explorer_SearchCompleted(object sender, SearchFilesEventArgs e)
        {
            lblSearchFolder.Text = "Search done!";
             


        }

        void Explorer_FileFound(object sender, SearchFilesEventArgs e)
        {

            //System.IO.FileInfo file = new System.IO.FileInfo(e.FilePath);
            //dgFiles.Rows.Add(file.Name, file.CreationTime, file.Length, file.FullName);
            try
            {
                dgFiles.DataSource = null;dgFiles.DataSource = Explorer.Table;
            }
            catch { }
            
            dgFiles.Columns[0].Width = 200;

        }

        void Explorer_CurrentSearchFolderChanged(object sender, SearchFilesEventArgs e)
        {
            lblSearchFolder.Text = e.FilePath;
             
        }

     

        void Player_StreamInfoCompleted(PlayerEventArgs e)
        {
            lblTitle.Text = Player.CurrentStream.CurrentSongAuthor + " - " + Player.CurrentStream.CurrentSongTitle;
        }

        private void Player_StateChanged(PlayerEventArgs e)
        {
            lblStatus.Text = e.State.ToString();
            switch (e.State)
            {
                case Player.PlayerState.Paused:
                    btnPlay.Image = Properties.Resources.play_16;
                    break;
                case Player.PlayerState.Playing:
                    btnPlay.Image = Properties.Resources.pause_16;
                    lblTitle.Text = Player.CurrentFile.Filename;
                    progress.Enabled = true;
                    break;
                case Player.PlayerState.Streaming:
                    btnPlay.Image = Properties.Resources.pause_16;
                    progress.Enabled = false;
                    lblTime.Text = "";
                    lblDuration.Text = "";
                    break;
                case Player.PlayerState.Stopped:
                    btnPlay.Image = Properties.Resources.play_16;
                    break;
            }
        }

        
        private void SongChanged(PlayerEventArgs e)
        {
           // lblTitle.Text = Player.CurrentFile.Filename;
        }

        

        private void tv1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            
            SelectedRows.Clear();
            string path = tv1.SelectedNode.Tag.ToString().ToLower();
            switch (path)
            {
                case "database":
                    //dgFiles.DataSource = Database.Table;
                    //Explorer.Location = Explorer.ExplorerLocation.Database;
                    //if (settings.DatabaseColumns != null)
                    //    ColumnsSettings.ApplySettings(dgFiles, settings.DatabaseColumns);
                    //break;
                case "radios":
                    dgFiles.DataSource = RadioManager.Table;
                    Explorer.Location = Explorer.ExplorerLocation.Radios;
                    if (settings.RadiosColumns  != null)
                        ColumnsSettings.ApplySettings(dgFiles, settings.RadiosColumns );
                    break;
                case "mycomputer": break;
                case "favorites": break;
                default:
                    dgFiles.DataSource = Explorer.GetFilesFromFolder(tv1.SelectedNode.Tag.ToString());
                    Explorer.Location = Explorer.ExplorerLocation.Folder;
                    if(settings.FilesColumns!=null)
                        ColumnsSettings.ApplySettings(dgFiles, settings.FilesColumns);
                    break;
            }

            btnExplorer.Text = tv1.SelectedNode.Text;

            
            

        }


        private void tv1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
          
        }

        private void tv1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
             
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Player.Stop();

        }

        private void dgFiles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgFiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            PlayFile(dgFiles.SelectedRows[0].Cells["url"].Value.ToString());
            tVis.Enabled = true;

        }

        public void PlayFile(string path)
        {
            Player.Play(path);
            //Database.AddFile(Player.CurrentFile);

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Player.CurrentFile != null)
            {          
                if (Player.State == Player.PlayerState.Playing)
                {

                    if (Player.CurrentPosition >= Player.CurrentFile.IntDuration)
                    {

                    }
                    else
                    {
                        Player.SetProgress(progress);
                    }
                }

            }

            if (Player.State == Player.PlayerState.Streaming)
            {
                Player.UpdateCurrentStreamInfo();
            }

        }

        private void trVol_Scroll(object sender, EventArgs e)
        {
            Player.Volume = trVol.Value;
        }

        private void progress_Scroll(object sender, EventArgs e)
        {
         
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            Player.PlayPause();

        }


        private void btnEQ_Click(object sender, EventArgs e)
        {
            //ShowEQForm();
            sc0.Panel2Collapsed = !sc0.Panel2Collapsed;
            
        }

        private frmEQ EQForm;
        private void ShowEQForm()
        {

            //EQForm = new frmEQ();
            //EQForm.EQ0.Value = Player.EQBand[0];
            //EQForm.EQ1.Value = Player.EQBand[1];
            //EQForm.EQ2.Value = Player.EQBand[2];
            //EQForm.EQ3.Value = Player.EQBand[3];
            //EQForm.EQ4.Value = Player.EQBand[4];
            //EQForm.EQ5.Value = Player.EQBand[5];
            //EQForm.EQ6.Value = Player.EQBand[6];
            //EQForm.EQ7.Value = Player.EQBand[7];
            //EQForm.EQ8.Value = Player.EQBand[8];
            //EQForm.EQ9.Value = Player.EQBand[9];
            //EQForm.Show();

        }

        private void tVis_Tick(object sender, EventArgs e)
        {
            if (Player.State == Player.PlayerState.Stopped | Player.State == Player.PlayerState.Paused) return;

            PicViz.BackgroundImage = Player.GetSpectrum(PicViz.Size);
           
        }

        private void PicViz_Click(object sender, EventArgs e)
        {
            Player.NextViz();

        }


        #region DriveDetector
                protected override void WndProc(ref Message m)
                {
                    switch (m.Msg)
                    {
                        case WM_DEVICECHANGE:

                            // The WParam value identifies what is occurring.
                            // n = (int)m.WParam;
                            switch (m.WParam.ToInt32())
                            {
                                case DBT_DEVICEARRIVAL: Explorer.UpdateDrives(MyComputerNode); break;
                                //case DBT_DEVICEQUERYREMOVE: Explorer.RemoveNotReadyDrives(MyComputerNode); break;
                                default:
                                    Explorer.RemoveNotReadyDrives(MyComputerNode);
                                    break;
                            }
                            break;
                    }
                    base.WndProc(ref m);
                }
                // Win32 constants
                private const int DBT_DEVTYP_DEVICEINTERFACE = 5;
                private const int DBT_DEVTYP_HANDLE = 6;
                private const int BROADCAST_QUERY_DENY = 0x424D5144;
                private const int WM_DEVICECHANGE = 0x0219;
                private const int DBT_DEVICEARRIVAL = 0x8000; // system detected a new device
                private const int DBT_DEVICEQUERYREMOVE = 0x8001;   // Preparing to remove (any program can disable the removal)
                private const int DBT_DEVICEREMOVECOMPLETE = 0x8004; // removed 
                private const int DBT_DEVTYP_VOLUME = 0x00000002;

                private void tv1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
                {
                    if (Explorer.IsSpecialFolder(tv1.SelectedNode.Tag.ToString())) return;
                    Explorer.AddSubFolders(tv1.SelectedNode, true);
                }  
        #endregion 

        private void txtSearch_Click(object sender, EventArgs e)
        {
            
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //Explorer.SearchDatabase(txtSearch.Text);
        }

        private void dgFiles_MouseUp(object sender, MouseEventArgs e)
        {
            //ako se dogodi bilo kakva radnja(resize,move)na ColumnHeaders spremi dg postavke 
            if (e.Y < dgFiles.ColumnHeadersHeight  ) SaveColumns();
            if (FileInfoForm.Visible) FileInfoForm.LoadFile(Explorer.SelectedFiles[0]);


        }

        private void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            txtSearch.Focus();
        }
        frmFileInfo FileInfoForm;
        private void tsDatabaseProperties_Click(object sender, EventArgs e)
        {

            FileInfoForm.LoadFile(Explorer.SelectedFiles[0]);


        }

        private bool Dragging = false;
        private void dgFiles_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView dg = dgFiles;
             
            DataGridView.HitTestInfo info=dg.HitTest(e.X,e.Y );

            if (info.RowIndex > -1)//nesto je selektirano
            {
                
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    if (dg.Rows[info.RowIndex].Selected)
                    {

                    }
                    else
                    {
                        dg.ClearSelection();
                        dg.Rows[info.RowIndex].Selected = true;
                    }
                    cmDatabase.Show(Cursor.Position);
                }
                else //ljevi gumb
                {
                    if (dg.Rows[info.RowIndex].Selected)
                    {       
                        Dragging = true;
                        //dg.DoDragDrop("dinamo", DragDropEffects.Copy);
                    }
                    else
                    {
                        dg.ClearSelection();
                        SelectedRows.Clear();
                        dg.Rows[info.RowIndex].Selected = true;
           
                    }
                    Explorer.GetSelectedFiles(dg);
                    
                    SaveSelectedRows(dg);

                        
                }
            }
        }

        List<DataGridViewRow> SelectedRows = new List<DataGridViewRow>();
        private void SaveSelectedRows(DataGridView dg)
        {
            SelectedRows.Clear();
            foreach (DataGridViewRow dr in dg.Rows)
            {
                if (dr.Selected) SelectedRows.Add(dr);
            }

            
        }
        private void ReselectRows()
        {
            foreach(DataGridViewRow dr in SelectedRows)
            {
                if(dr.Index !=-1)
                dr.Selected = true;

            }
        }
        private int GetStringWidth(string str, Font strFont) {

            System.Drawing.Graphics g = this.CreateGraphics();
            int strW =(int) g.MeasureString(str, strFont).Width;
            g.Dispose();
            return strW;

        }
        private void dgFiles_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1) return;
            DataGridView dg = dgFiles;
             
                int w = GetStringWidth(dg.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), dg.DefaultCellStyle.Font);
                if (e.X <= w) Dragging=true;
            
        }

        private void dgFiles_MouseMove(object sender, MouseEventArgs e)
        {
            
            DataGridView dg = dgFiles;
            DataGridView.HitTestInfo info = dg.HitTest(e.X, e.Y);
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            { 
                if (Dragging)
                {
                    if (info.RowIndex > -1)
                    {
                        ReselectRows();
                         
                        dg.DoDragDrop(new DataObject(DataFormats.FileDrop, Explorer.SelectedFiles), DragDropEffects.Copy);
                        Dragging = false;
                    }
                }
            }


        }

        private void btnExplorer_Click(object sender, EventArgs e)
        {
           
        }

        private void btnSearch_ButtonClick(object sender, EventArgs e)
        {
             
            string strSearch=txtSearch.Text ;

            switch (Explorer.Location)
            {
                case Explorer.ExplorerLocation.Folder:
                    dgFiles.DataSource = Explorer.Table;
                     
                    //dgFiles.Columns.Clear();
                    //DataGridViewColumn c = new DataGridViewTextBoxColumn();
                    //            c.ValueType = typeof(string);
                    //            c.HeaderText = "Filename";
                    //            DataGridViewColumn c2 = new DataGridViewTextBoxColumn();
                    //            c2.ValueType = typeof(DateTime );
                    //            c2.HeaderText = "DateCreated";
                    //            DataGridViewColumn c3 = new DataGridViewTextBoxColumn();
                    //            c3.ValueType = typeof(long);
                    //            c3.HeaderText = "Size";
                    //            DataGridViewColumn c4 = new DataGridViewTextBoxColumn();
                    //            c4.ValueType = typeof(String);
                    //            c4.HeaderText = "Url";
                    //            c4.Name = "Url";

                    //            dgFiles.Columns.Add(c);
                    //            dgFiles.Columns.Add(c2);
                    //            dgFiles.Columns.Add(c3);
                    //            dgFiles.Columns.Add(c4);

                    Explorer.Search(strSearch,tv1.SelectedNode.Tag.ToString());
                    break;
                case Explorer.ExplorerLocation.Database:
                    dgFiles.DataSource = Database.Search(strSearch);
                    break;
                case Explorer.ExplorerLocation.Radios:
                    SearchProgress.Visible = true;
                    dgFiles.DataSource = RadioManager.Table;
                    RadioManager.Search(txtSearch.Text);
                    break;
            }
           
      
        }

        void RadioManager_SearchCompleted(object sender, RadiosEventArgs e)
        {
            //dgFiles.Refresh();
            dgFiles.DataSource = null;
            SearchProgress.Visible = false;
            dgFiles.DataSource = RadioManager.Table;
            

        }

        private void tv1_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("FileDrop"))
            {
                e.Effect = DragDropEffects.Copy;

                Point pt = tv1.PointToClient(new Point(e.X, e.Y ));
                TreeNode node= tv1.GetNodeAt(pt);
                if (node != null)
                    DragOverNode = node;

            }
            else
            {
                e.Effect = DragDropEffects.None;
            }

           
        }

        private TreeNode _DragOverNode;
        public TreeNode DragOverNode {
            get
            {
                return _DragOverNode;
            } 
            set 
            {
                if (value == null) { if (_DragOverNode != null) _DragOverNode.BackColor = tv1.BackColor; DragOverNode = null; return; }
                if (value.Equals(_DragOverNode)) return;
                if (_DragOverNode != null) _DragOverNode.BackColor = tv1.BackColor;
                _DragOverNode = value;
                if (_DragOverNode != null) _DragOverNode.BackColor = Color.LightSlateGray;


            }
        }

        private void tv1_DragDrop(object sender, DragEventArgs e)
        {

            DragOverNode = null;

            string[] files = (string[])(e.Data.GetData("FileDrop", true));
            Point pt = tv1.PointToClient(new Point(e.X, e.Y));
            TreeNode node = tv1.GetNodeAt(pt);
            if (node == null) return;
            string dest =node.Tag.ToString();


            if (Explorer.IsSpecialFolder(dest))
            {

            }
            else
            {
                ShellFileOperation.DoFileOperation(files, dest, "Dinamo", ShellFileOperation.FileOp.Copy, this);
            }
             

        }

        private void tv1_DragLeave(object sender, EventArgs e)
        {
            if(DragOverNode!=null)
            DragOverNode.BackColor = tv1.BackColor;


        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //MessageBox.Show(Explorer.SelectedFiles[0]);

            //ShellFileOperation.ShowFileProperties(Explorer.SelectedFiles[0]);
            ShellFileOperation.ShowFileProperties(tv1.SelectedNode.Tag.ToString());


        }

        private void tv1_MouseUp(object sender, MouseEventArgs e)
        {
            if(e.Button==System.Windows.Forms.MouseButtons.Right)
            cmFolder.Show(Cursor.Position);
        }

        private void progress_MouseDown(object sender, MouseEventArgs e)
        {
            float step =Convert.ToSingle(Player.CurrentFile.IntDuration)/ Convert.ToSingle(progress.Width) ;
            int dur = Convert.ToInt32(step * e.X);

            this.Text = dur.ToString();

            Player.CurrentPosition =  dur ;
            Player.SetProgress(progress);

        }

        private void progress_Click(object sender, EventArgs e)
        {

        }

        private void EQScroll(object sender, EventArgs e)
        {
            TrackBar EQ = (TrackBar)sender;
            if (EQ.Value == 1 | EQ.Value == -1) EQ.Value = 0;
            int intBand = Convert.ToInt32(EQ.Tag);
            Player.EQBandValue[intBand] = EQ.Value;
            Player.UpdateEQ(intBand);
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            
        }

        private void btnPlayerEQ_Click(object sender, EventArgs e)
        {

        }

        private void btnSavePreset_Click(object sender, EventArgs e)
        {
            InputBox box = new InputBox("Enter name");
            if (box.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EQPreset p = new EQPreset();
                p.Name = box.txtInput.Text;
                p.Values[0] = Player.EQBandValue[0];
                p.Values[1] = Player.EQBandValue[1];
                p.Values[2] = Player.EQBandValue[2];
                p.Values[3] = Player.EQBandValue[3];
                p.Values[4] = Player.EQBandValue[4];
                p.Values[5] = Player.EQBandValue[5];
                p.Values[6] = Player.EQBandValue[6];
                p.Values[7] = Player.EQBandValue[7];
                p.Values[8] = Player.EQBandValue[8];
                p.Values[9] = Player.EQBandValue[9];
                settings.EQPresets.Add(p);

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            settings.Volume = Player.Volume;
          
            settings.nodeSettings = new List<NodeSettings>();
            foreach (TreeNode n in tv1.Nodes)
            {
                settings.nodeSettings.Add(NodeSettings.GetNodeSettings(n));

            }
            settings.Save();

        }

        private void lstEQPresets_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyEQPreset(settings.EQPresets[lstEQPresets.SelectedIndex]);
            
        }



        private void ApplyEQPreset(EQPreset p)
        {
            EQ0.Value = p.Values[0];
            EQ1.Value = p.Values[1];
            EQ2.Value = p.Values[2];
            EQ3.Value = p.Values[3];
            EQ4.Value = p.Values[4];
            EQ5.Value = p.Values[5];
            EQ6.Value = p.Values[6];
            EQ7.Value = p.Values[7];
            EQ8.Value = p.Values[8];
            EQ9.Value = p.Values[9];
            Player.ApplyPreset(p);
        }

        private void btnFavorites_Click(object sender, EventArgs e)
        {
            tv1.Nodes.Clear();
            tv1.Nodes.Add(Explorer.NodeFavorites);
        }

        private void addToFavoritesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Explorer.NodeFavorites.Nodes.Add((TreeNode) tv1.SelectedNode.Clone());

        }

        private void dgFiles_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            //SaveColumns();
        }


        private void SaveColumns()
        {
            switch (Explorer.Location)
            {
                case Explorer.ExplorerLocation.Folder:
                    settings.FilesColumns = ColumnsSettings.GetSettings(dgFiles);
                    break;
                case Explorer.ExplorerLocation.Database:
                    settings.DatabaseColumns = ColumnsSettings.GetSettings(dgFiles);
                    break;
                case Explorer.ExplorerLocation.Radios:
                    settings.RadiosColumns = ColumnsSettings.GetSettings(dgFiles);
                    break;

            }

        }

        private void btnPlayerTag_Click(object sender, EventArgs e)
        {
            Database.AddFile(Player.CurrentFile);

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tv0_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tv0.SelectedNode == null) return;
            tv1.Nodes.Clear();
            string path = tv0.SelectedNode.Text.ToString().ToLower();
            switch (path)
            {
                case "database":

                    break;
                    
                case "radios":
                    dgFiles.DataSource = RadioManager.Table;
                    Explorer.Location = Explorer.ExplorerLocation.Radios;
                    if (settings.RadiosColumns  != null)
                        ColumnsSettings.ApplySettings(dgFiles, settings.RadiosColumns );
                    btnExplorer.Text = "Search radios";
                    break;
                case "mycomputer":
                    foreach (TreeNode n in MyComputerNode.Nodes)
                    {
                        tv1.Nodes.Add(n);
                    }
                break;
                    
                case "favorites":
                    foreach (TreeNode n in Explorer.NodeFavorites.Nodes)
                    {
                        tv1.Nodes.Add(n);
                    }
                    break;
                default:
                    dgFiles.DataSource = Explorer.GetFilesFromFolder(tv1.SelectedNode.Tag.ToString());
                    Explorer.Location = Explorer.ExplorerLocation.Folder;
                    if (settings.FilesColumns != null)
                        ColumnsSettings.ApplySettings(dgFiles, settings.FilesColumns);
                    break;
            }

        }

    
    }
}
