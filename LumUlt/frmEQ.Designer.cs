﻿namespace LumUlt
{
    partial class frmEQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQ));
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.Label25 = new System.Windows.Forms.Label();
            this.btnRemovePreset = new System.Windows.Forms.Button();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.btnSavePreset = new System.Windows.Forms.Button();
            this.cbPresets = new System.Windows.Forms.ComboBox();
            this.Label24 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.EQBand = new System.Windows.Forms.TrackBar();
            this.EQ0 = new System.Windows.Forms.TrackBar();
            this.EQ1 = new System.Windows.Forms.TrackBar();
            this.EQ2 = new System.Windows.Forms.TrackBar();
            this.EQ3 = new System.Windows.Forms.TrackBar();
            this.Label14 = new System.Windows.Forms.Label();
            this.EQ4 = new System.Windows.Forms.TrackBar();
            this.Label15 = new System.Windows.Forms.Label();
            this.EQ5 = new System.Windows.Forms.TrackBar();
            this.Label16 = new System.Windows.Forms.Label();
            this.EQ6 = new System.Windows.Forms.TrackBar();
            this.Label17 = new System.Windows.Forms.Label();
            this.EQ7 = new System.Windows.Forms.TrackBar();
            this.Label18 = new System.Windows.Forms.Label();
            this.EQ8 = new System.Windows.Forms.TrackBar();
            this.Label19 = new System.Windows.Forms.Label();
            this.EQ9 = new System.Windows.Forms.TrackBar();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.trChorus = new System.Windows.Forms.TrackBar();
            this.btnKillBass = new System.Windows.Forms.Button();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.btnReverb = new System.Windows.Forms.Button();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label27 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.trReverbReverbMix = new System.Windows.Forms.TrackBar();
            this.trReverbInGain = new System.Windows.Forms.TrackBar();
            this.trReverbReverbTime = new System.Windows.Forms.TrackBar();
            this.trReverbHighFreqRTRatio = new System.Windows.Forms.TrackBar();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.btnEcho = new System.Windows.Forms.Button();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.trEchoRightDelay = new System.Windows.Forms.TrackBar();
            this.trEchoLeftDelay = new System.Windows.Forms.TrackBar();
            this.trEchoWetDry = new System.Windows.Forms.TrackBar();
            this.trEchoFeedback = new System.Windows.Forms.TrackBar();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFlanger = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.trFlangerFeedback = new System.Windows.Forms.TrackBar();
            this.trFlangerWetDry = new System.Windows.Forms.TrackBar();
            this.trFlangerDepth = new System.Windows.Forms.TrackBar();
            this.trFlangerFrequency = new System.Windows.Forms.TrackBar();
            this.trFlangerDelay = new System.Windows.Forms.TrackBar();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.tbStart = new System.Windows.Forms.TrackBar();
            this.Label29 = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.GroupBox6 = new System.Windows.Forms.GroupBox();
            this.lblOUT = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.tbOut = new System.Windows.Forms.TrackBar();
            this.lblIN = new System.Windows.Forms.Label();
            this.Label31 = new System.Windows.Forms.Label();
            this.tbIn = new System.Windows.Forms.TrackBar();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.gbBalance = new System.Windows.Forms.Label();
            this.trBalance = new System.Windows.Forms.TrackBar();
            this.TabPage5 = new System.Windows.Forms.TabPage();
            this.trSystemVolume = new System.Windows.Forms.TrackBar();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EQBand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ9)).BeginInit();
            this.TabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trChorus)).BeginInit();
            this.GroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbReverbMix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbInGain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbReverbTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbHighFreqRTRatio)).BeginInit();
            this.GroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoRightDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoLeftDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoWetDry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoFeedback)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerFeedback)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerWetDry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerDelay)).BeginInit();
            this.TabPage3.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStart)).BeginInit();
            this.GroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbIn)).BeginInit();
            this.TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trBalance)).BeginInit();
            this.TabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trSystemVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.TabPage3);
            this.TabControl1.Controls.Add(this.TabPage4);
            this.TabControl1.Controls.Add(this.TabPage5);
            this.TabControl1.Location = new System.Drawing.Point(-18, -53);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(552, 278);
            this.TabControl1.TabIndex = 9;
            // 
            // TabPage1
            // 
            this.TabPage1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TabPage1.Controls.Add(this.GroupBox2);
            this.TabPage1.Location = new System.Drawing.Point(4, 22);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(544, 252);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Equalizer";
            // 
            // GroupBox2
            // 
            this.GroupBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.GroupBox2.Controls.Add(this.Label25);
            this.GroupBox2.Controls.Add(this.btnRemovePreset);
            this.GroupBox2.Controls.Add(this.Label26);
            this.GroupBox2.Controls.Add(this.Label11);
            this.GroupBox2.Controls.Add(this.btnSavePreset);
            this.GroupBox2.Controls.Add(this.cbPresets);
            this.GroupBox2.Controls.Add(this.Label24);
            this.GroupBox2.Controls.Add(this.Label13);
            this.GroupBox2.Controls.Add(this.EQBand);
            this.GroupBox2.Controls.Add(this.EQ0);
            this.GroupBox2.Controls.Add(this.EQ1);
            this.GroupBox2.Controls.Add(this.EQ2);
            this.GroupBox2.Controls.Add(this.EQ3);
            this.GroupBox2.Controls.Add(this.Label14);
            this.GroupBox2.Controls.Add(this.EQ4);
            this.GroupBox2.Controls.Add(this.Label15);
            this.GroupBox2.Controls.Add(this.EQ5);
            this.GroupBox2.Controls.Add(this.Label16);
            this.GroupBox2.Controls.Add(this.EQ6);
            this.GroupBox2.Controls.Add(this.Label17);
            this.GroupBox2.Controls.Add(this.EQ7);
            this.GroupBox2.Controls.Add(this.Label18);
            this.GroupBox2.Controls.Add(this.EQ8);
            this.GroupBox2.Controls.Add(this.Label19);
            this.GroupBox2.Controls.Add(this.EQ9);
            this.GroupBox2.Controls.Add(this.Label20);
            this.GroupBox2.Controls.Add(this.Label21);
            this.GroupBox2.Controls.Add(this.Label22);
            this.GroupBox2.Controls.Add(this.Label23);
            this.GroupBox2.Location = new System.Drawing.Point(17, 32);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(531, 224);
            this.GroupBox2.TabIndex = 6;
            this.GroupBox2.TabStop = false;
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label25.Location = new System.Drawing.Point(66, 121);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(19, 12);
            this.Label25.TabIndex = 3;
            this.Label25.Text = " + 0";
            // 
            // btnRemovePreset
            // 
            this.btnRemovePreset.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnRemovePreset.Location = new System.Drawing.Point(199, 16);
            this.btnRemovePreset.Name = "btnRemovePreset";
            this.btnRemovePreset.Size = new System.Drawing.Size(59, 22);
            this.btnRemovePreset.TabIndex = 5;
            this.btnRemovePreset.Text = "Remove";
            this.btnRemovePreset.UseVisualStyleBackColor = true;
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label26.Location = new System.Drawing.Point(18, 205);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(24, 12);
            this.Label26.TabIndex = 3;
            this.Label26.Text = "PRE";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(4, 21);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(37, 13);
            this.Label11.TabIndex = 4;
            this.Label11.Text = "Preset";
            // 
            // btnSavePreset
            // 
            this.btnSavePreset.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnSavePreset.Location = new System.Drawing.Point(417, 16);
            this.btnSavePreset.Name = "btnSavePreset";
            this.btnSavePreset.Size = new System.Drawing.Size(84, 22);
            this.btnSavePreset.TabIndex = 5;
            this.btnSavePreset.Text = "Save preset";
            this.btnSavePreset.UseVisualStyleBackColor = true;
            // 
            // cbPresets
            // 
            this.cbPresets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPresets.FormattingEnabled = true;
            this.cbPresets.Items.AddRange(new object[] {
            "Default",
            "Kill Bass",
            "Club"});
            this.cbPresets.Location = new System.Drawing.Point(50, 17);
            this.cbPresets.Name = "cbPresets";
            this.cbPresets.Size = new System.Drawing.Size(143, 21);
            this.cbPresets.TabIndex = 3;
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label24.Location = new System.Drawing.Point(66, 179);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(18, 12);
            this.Label24.TabIndex = 3;
            this.Label24.Text = "-16";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(66, 61);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(20, 12);
            this.Label13.TabIndex = 3;
            this.Label13.Text = "+16";
            // 
            // EQBand
            // 
            this.EQBand.AutoSize = false;
            this.EQBand.Enabled = false;
            this.EQBand.Location = new System.Drawing.Point(20, 52);
            this.EQBand.Maximum = 12;
            this.EQBand.Minimum = -12;
            this.EQBand.Name = "EQBand";
            this.EQBand.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQBand.Size = new System.Drawing.Size(40, 149);
            this.EQBand.TabIndex = 1;
            this.EQBand.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQBand.Scroll += new System.EventHandler(this.EQBand_Scroll);
            // 
            // EQ0
            // 
            this.EQ0.AutoSize = false;
            this.EQ0.Location = new System.Drawing.Point(92, 52);
            this.EQ0.Maximum = 15;
            this.EQ0.Minimum = -15;
            this.EQ0.Name = "EQ0";
            this.EQ0.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ0.Size = new System.Drawing.Size(36, 150);
            this.EQ0.TabIndex = 1;
            this.EQ0.Tag = "0";
            this.EQ0.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ0.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ1
            // 
            this.EQ1.AutoSize = false;
            this.EQ1.Location = new System.Drawing.Point(134, 52);
            this.EQ1.Maximum = 15;
            this.EQ1.Minimum = -15;
            this.EQ1.Name = "EQ1";
            this.EQ1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ1.Size = new System.Drawing.Size(36, 150);
            this.EQ1.TabIndex = 1;
            this.EQ1.Tag = "1";
            this.EQ1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ1.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ2
            // 
            this.EQ2.AutoSize = false;
            this.EQ2.Location = new System.Drawing.Point(176, 52);
            this.EQ2.Maximum = 15;
            this.EQ2.Minimum = -15;
            this.EQ2.Name = "EQ2";
            this.EQ2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ2.Size = new System.Drawing.Size(36, 150);
            this.EQ2.TabIndex = 1;
            this.EQ2.Tag = "2";
            this.EQ2.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ2.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ3
            // 
            this.EQ3.AutoSize = false;
            this.EQ3.Location = new System.Drawing.Point(215, 52);
            this.EQ3.Maximum = 15;
            this.EQ3.Minimum = -15;
            this.EQ3.Name = "EQ3";
            this.EQ3.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ3.Size = new System.Drawing.Size(36, 150);
            this.EQ3.TabIndex = 1;
            this.EQ3.Tag = "3";
            this.EQ3.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ3.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // Label14
            // 
            this.Label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label14.Location = new System.Drawing.Point(425, 202);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(25, 20);
            this.Label14.TabIndex = 2;
            this.Label14.Text = "14K";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EQ4
            // 
            this.EQ4.AutoSize = false;
            this.EQ4.Location = new System.Drawing.Point(255, 52);
            this.EQ4.Maximum = 15;
            this.EQ4.Minimum = -15;
            this.EQ4.Name = "EQ4";
            this.EQ4.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ4.Size = new System.Drawing.Size(36, 150);
            this.EQ4.TabIndex = 1;
            this.EQ4.Tag = "4";
            this.EQ4.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ4.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // Label15
            // 
            this.Label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.Location = new System.Drawing.Point(465, 202);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(25, 20);
            this.Label15.TabIndex = 2;
            this.Label15.Text = "16K";
            this.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EQ5
            // 
            this.EQ5.AutoSize = false;
            this.EQ5.Location = new System.Drawing.Point(291, 52);
            this.EQ5.Maximum = 15;
            this.EQ5.Minimum = -15;
            this.EQ5.Name = "EQ5";
            this.EQ5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ5.Size = new System.Drawing.Size(36, 150);
            this.EQ5.TabIndex = 1;
            this.EQ5.Tag = "5";
            this.EQ5.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ5.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // Label16
            // 
            this.Label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(385, 202);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(25, 20);
            this.Label16.TabIndex = 2;
            this.Label16.Text = "12K";
            this.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EQ6
            // 
            this.EQ6.AutoSize = false;
            this.EQ6.Location = new System.Drawing.Point(333, 52);
            this.EQ6.Maximum = 15;
            this.EQ6.Minimum = -15;
            this.EQ6.Name = "EQ6";
            this.EQ6.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ6.Size = new System.Drawing.Size(36, 150);
            this.EQ6.TabIndex = 1;
            this.EQ6.Tag = "6";
            this.EQ6.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ6.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // Label17
            // 
            this.Label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label17.Location = new System.Drawing.Point(345, 202);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(25, 20);
            this.Label17.TabIndex = 2;
            this.Label17.Text = "6K";
            this.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EQ7
            // 
            this.EQ7.AutoSize = false;
            this.EQ7.Location = new System.Drawing.Point(375, 52);
            this.EQ7.Maximum = 15;
            this.EQ7.Minimum = -15;
            this.EQ7.Name = "EQ7";
            this.EQ7.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ7.Size = new System.Drawing.Size(36, 150);
            this.EQ7.TabIndex = 1;
            this.EQ7.Tag = "7";
            this.EQ7.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ7.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // Label18
            // 
            this.Label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label18.Location = new System.Drawing.Point(305, 202);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(25, 20);
            this.Label18.TabIndex = 2;
            this.Label18.Text = "3K";
            this.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EQ8
            // 
            this.EQ8.AutoSize = false;
            this.EQ8.Location = new System.Drawing.Point(417, 52);
            this.EQ8.Maximum = 15;
            this.EQ8.Minimum = -15;
            this.EQ8.Name = "EQ8";
            this.EQ8.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ8.Size = new System.Drawing.Size(36, 150);
            this.EQ8.TabIndex = 1;
            this.EQ8.Tag = "8";
            this.EQ8.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ8.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // Label19
            // 
            this.Label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label19.Location = new System.Drawing.Point(265, 202);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(25, 20);
            this.Label19.TabIndex = 2;
            this.Label19.Text = "1K";
            this.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EQ9
            // 
            this.EQ9.AutoSize = false;
            this.EQ9.Location = new System.Drawing.Point(459, 52);
            this.EQ9.Maximum = 15;
            this.EQ9.Minimum = -15;
            this.EQ9.Name = "EQ9";
            this.EQ9.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ9.Size = new System.Drawing.Size(36, 150);
            this.EQ9.TabIndex = 1;
            this.EQ9.Tag = "9";
            this.EQ9.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ9.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // Label20
            // 
            this.Label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label20.Location = new System.Drawing.Point(225, 202);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(25, 20);
            this.Label20.TabIndex = 2;
            this.Label20.Text = "600";
            this.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label21
            // 
            this.Label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label21.Location = new System.Drawing.Point(105, 202);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(25, 20);
            this.Label21.TabIndex = 2;
            this.Label21.Text = "80";
            this.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label22
            // 
            this.Label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label22.Location = new System.Drawing.Point(185, 202);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(25, 20);
            this.Label22.TabIndex = 2;
            this.Label22.Text = "320";
            this.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label23
            // 
            this.Label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label23.Location = new System.Drawing.Point(145, 202);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(25, 20);
            this.Label23.TabIndex = 2;
            this.Label23.Text = "160";
            this.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TabPage2
            // 
            this.TabPage2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TabPage2.Controls.Add(this.trChorus);
            this.TabPage2.Controls.Add(this.btnKillBass);
            this.TabPage2.Controls.Add(this.GroupBox4);
            this.TabPage2.Controls.Add(this.GroupBox3);
            this.TabPage2.Controls.Add(this.GroupBox1);
            this.TabPage2.Location = new System.Drawing.Point(4, 22);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(544, 252);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "Effects";
            // 
            // trChorus
            // 
            this.trChorus.Location = new System.Drawing.Point(215, 191);
            this.trChorus.Name = "trChorus";
            this.trChorus.Size = new System.Drawing.Size(139, 45);
            this.trChorus.TabIndex = 6;
            // 
            // btnKillBass
            // 
            this.btnKillBass.Location = new System.Drawing.Point(125, 210);
            this.btnKillBass.Name = "btnKillBass";
            this.btnKillBass.Size = new System.Drawing.Size(75, 48);
            this.btnKillBass.TabIndex = 5;
            this.btnKillBass.Text = "KillBass";
            this.btnKillBass.UseVisualStyleBackColor = true;
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.btnReverb);
            this.GroupBox4.Controls.Add(this.Label7);
            this.GroupBox4.Controls.Add(this.Label12);
            this.GroupBox4.Controls.Add(this.Label27);
            this.GroupBox4.Controls.Add(this.Label28);
            this.GroupBox4.Controls.Add(this.trReverbReverbMix);
            this.GroupBox4.Controls.Add(this.trReverbInGain);
            this.GroupBox4.Controls.Add(this.trReverbReverbTime);
            this.GroupBox4.Controls.Add(this.trReverbHighFreqRTRatio);
            this.GroupBox4.Location = new System.Drawing.Point(7, 165);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(174, 92);
            this.GroupBox4.TabIndex = 0;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Reverb";
            // 
            // btnReverb
            // 
            this.btnReverb.Location = new System.Drawing.Point(8, 57);
            this.btnReverb.Name = "btnReverb";
            this.btnReverb.Size = new System.Drawing.Size(38, 25);
            this.btnReverb.TabIndex = 3;
            this.btnReverb.Text = "ON";
            this.btnReverb.UseVisualStyleBackColor = true;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(6, 19);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(49, 11);
            this.Label7.TabIndex = 2;
            this.Label7.Text = "ReverbMix";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(6, 133);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(38, 11);
            this.Label12.TabIndex = 2;
            this.Label12.Text = "InGaing";
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label27.Location = new System.Drawing.Point(6, 38);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(56, 11);
            this.Label27.TabIndex = 2;
            this.Label27.Text = "ReverbTime";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label28.Location = new System.Drawing.Point(6, 114);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(76, 11);
            this.Label28.TabIndex = 2;
            this.Label28.Text = "HighFreqRTRatio";
            // 
            // trReverbReverbMix
            // 
            this.trReverbReverbMix.AutoSize = false;
            this.trReverbReverbMix.Location = new System.Drawing.Point(71, 14);
            this.trReverbReverbMix.Maximum = 0;
            this.trReverbReverbMix.Minimum = -96;
            this.trReverbReverbMix.Name = "trReverbReverbMix";
            this.trReverbReverbMix.Size = new System.Drawing.Size(97, 35);
            this.trReverbReverbMix.TabIndex = 1;
            this.trReverbReverbMix.Tag = "Echo";
            this.trReverbReverbMix.TickFrequency = 100;
            // 
            // trReverbInGain
            // 
            this.trReverbInGain.AutoSize = false;
            this.trReverbInGain.Location = new System.Drawing.Point(83, 133);
            this.trReverbInGain.Maximum = 0;
            this.trReverbInGain.Minimum = -96;
            this.trReverbInGain.Name = "trReverbInGain";
            this.trReverbInGain.Size = new System.Drawing.Size(85, 13);
            this.trReverbInGain.TabIndex = 1;
            this.trReverbInGain.Tag = "Echo";
            this.trReverbInGain.TickFrequency = 100;
            // 
            // trReverbReverbTime
            // 
            this.trReverbReverbTime.AutoSize = false;
            this.trReverbReverbTime.Location = new System.Drawing.Point(68, 52);
            this.trReverbReverbTime.Maximum = 3000;
            this.trReverbReverbTime.Minimum = 1;
            this.trReverbReverbTime.Name = "trReverbReverbTime";
            this.trReverbReverbTime.Size = new System.Drawing.Size(100, 34);
            this.trReverbReverbTime.TabIndex = 1;
            this.trReverbReverbTime.Tag = "Echo";
            this.trReverbReverbTime.TickFrequency = 10;
            this.trReverbReverbTime.Value = 1;
            // 
            // trReverbHighFreqRTRatio
            // 
            this.trReverbHighFreqRTRatio.AutoSize = false;
            this.trReverbHighFreqRTRatio.Location = new System.Drawing.Point(83, 114);
            this.trReverbHighFreqRTRatio.Maximum = 999;
            this.trReverbHighFreqRTRatio.Minimum = 1;
            this.trReverbHighFreqRTRatio.Name = "trReverbHighFreqRTRatio";
            this.trReverbHighFreqRTRatio.Size = new System.Drawing.Size(85, 13);
            this.trReverbHighFreqRTRatio.TabIndex = 1;
            this.trReverbHighFreqRTRatio.Tag = "Echo";
            this.trReverbHighFreqRTRatio.TickFrequency = 10;
            this.trReverbHighFreqRTRatio.Value = 1;
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.btnEcho);
            this.GroupBox3.Controls.Add(this.Label6);
            this.GroupBox3.Controls.Add(this.Label8);
            this.GroupBox3.Controls.Add(this.Label9);
            this.GroupBox3.Controls.Add(this.Label10);
            this.GroupBox3.Controls.Add(this.trEchoRightDelay);
            this.GroupBox3.Controls.Add(this.trEchoLeftDelay);
            this.GroupBox3.Controls.Add(this.trEchoWetDry);
            this.GroupBox3.Controls.Add(this.trEchoFeedback);
            this.GroupBox3.Location = new System.Drawing.Point(186, 3);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(174, 124);
            this.GroupBox3.TabIndex = 0;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Echo";
            // 
            // btnEcho
            // 
            this.btnEcho.Location = new System.Drawing.Point(6, 95);
            this.btnEcho.Name = "btnEcho";
            this.btnEcho.Size = new System.Drawing.Size(40, 23);
            this.btnEcho.TabIndex = 3;
            this.btnEcho.Text = "ON";
            this.btnEcho.UseVisualStyleBackColor = true;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(6, 57);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(52, 11);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "Right delay";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(6, 38);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(46, 11);
            this.Label8.TabIndex = 2;
            this.Label8.Text = "Left delay";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(6, 76);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(40, 11);
            this.Label9.TabIndex = 2;
            this.Label9.Text = "Wet/Dry";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(6, 19);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(45, 11);
            this.Label10.TabIndex = 2;
            this.Label10.Text = "Feedback";
            // 
            // trEchoRightDelay
            // 
            this.trEchoRightDelay.AutoSize = false;
            this.trEchoRightDelay.Location = new System.Drawing.Point(83, 57);
            this.trEchoRightDelay.Maximum = 2000;
            this.trEchoRightDelay.Minimum = 1;
            this.trEchoRightDelay.Name = "trEchoRightDelay";
            this.trEchoRightDelay.Size = new System.Drawing.Size(85, 13);
            this.trEchoRightDelay.TabIndex = 1;
            this.trEchoRightDelay.Tag = "Echo";
            this.trEchoRightDelay.TickFrequency = 100;
            this.trEchoRightDelay.Value = 1;
            // 
            // trEchoLeftDelay
            // 
            this.trEchoLeftDelay.AutoSize = false;
            this.trEchoLeftDelay.Location = new System.Drawing.Point(83, 38);
            this.trEchoLeftDelay.Maximum = 2000;
            this.trEchoLeftDelay.Minimum = 1;
            this.trEchoLeftDelay.Name = "trEchoLeftDelay";
            this.trEchoLeftDelay.Size = new System.Drawing.Size(85, 13);
            this.trEchoLeftDelay.TabIndex = 1;
            this.trEchoLeftDelay.Tag = "Echo";
            this.trEchoLeftDelay.TickFrequency = 100;
            this.trEchoLeftDelay.Value = 1;
            // 
            // trEchoWetDry
            // 
            this.trEchoWetDry.AutoSize = false;
            this.trEchoWetDry.Location = new System.Drawing.Point(55, 76);
            this.trEchoWetDry.Maximum = 100;
            this.trEchoWetDry.Minimum = 1;
            this.trEchoWetDry.Name = "trEchoWetDry";
            this.trEchoWetDry.Size = new System.Drawing.Size(113, 28);
            this.trEchoWetDry.TabIndex = 1;
            this.trEchoWetDry.Tag = "Echo";
            this.trEchoWetDry.TickFrequency = 10;
            this.trEchoWetDry.Value = 1;
            // 
            // trEchoFeedback
            // 
            this.trEchoFeedback.AutoSize = false;
            this.trEchoFeedback.Location = new System.Drawing.Point(83, 19);
            this.trEchoFeedback.Maximum = 100;
            this.trEchoFeedback.Name = "trEchoFeedback";
            this.trEchoFeedback.Size = new System.Drawing.Size(85, 13);
            this.trEchoFeedback.TabIndex = 1;
            this.trEchoFeedback.Tag = "Echo";
            this.trEchoFeedback.TickFrequency = 10;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.btnFlanger);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.trFlangerFeedback);
            this.GroupBox1.Controls.Add(this.trFlangerWetDry);
            this.GroupBox1.Controls.Add(this.trFlangerDepth);
            this.GroupBox1.Controls.Add(this.trFlangerFrequency);
            this.GroupBox1.Controls.Add(this.trFlangerDelay);
            this.GroupBox1.Location = new System.Drawing.Point(6, 3);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(174, 170);
            this.GroupBox1.TabIndex = 0;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Flanger";
            // 
            // btnFlanger
            // 
            this.btnFlanger.Location = new System.Drawing.Point(6, 39);
            this.btnFlanger.Name = "btnFlanger";
            this.btnFlanger.Size = new System.Drawing.Size(41, 23);
            this.btnFlanger.TabIndex = 3;
            this.btnFlanger.Text = "ON";
            this.btnFlanger.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(6, 93);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(45, 11);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "Feedback";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(6, 19);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(40, 11);
            this.Label5.TabIndex = 2;
            this.Label5.Text = "Wet/Dry";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(6, 74);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(30, 11);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Depth";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(6, 112);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 11);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "Freqouency";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(13, 131);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(31, 11);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "Speed";
            // 
            // trFlangerFeedback
            // 
            this.trFlangerFeedback.AutoSize = false;
            this.trFlangerFeedback.Location = new System.Drawing.Point(83, 93);
            this.trFlangerFeedback.Maximum = 99;
            this.trFlangerFeedback.Minimum = -99;
            this.trFlangerFeedback.Name = "trFlangerFeedback";
            this.trFlangerFeedback.Size = new System.Drawing.Size(85, 13);
            this.trFlangerFeedback.TabIndex = 1;
            // 
            // trFlangerWetDry
            // 
            this.trFlangerWetDry.AutoSize = false;
            this.trFlangerWetDry.Location = new System.Drawing.Point(66, 12);
            this.trFlangerWetDry.Maximum = 100;
            this.trFlangerWetDry.Name = "trFlangerWetDry";
            this.trFlangerWetDry.Size = new System.Drawing.Size(102, 30);
            this.trFlangerWetDry.TabIndex = 1;
            this.trFlangerWetDry.TickFrequency = 10;
            // 
            // trFlangerDepth
            // 
            this.trFlangerDepth.AutoSize = false;
            this.trFlangerDepth.Location = new System.Drawing.Point(83, 74);
            this.trFlangerDepth.Maximum = 100;
            this.trFlangerDepth.Name = "trFlangerDepth";
            this.trFlangerDepth.Size = new System.Drawing.Size(85, 13);
            this.trFlangerDepth.TabIndex = 1;
            // 
            // trFlangerFrequency
            // 
            this.trFlangerFrequency.AutoSize = false;
            this.trFlangerFrequency.Location = new System.Drawing.Point(83, 112);
            this.trFlangerFrequency.Maximum = 50;
            this.trFlangerFrequency.Name = "trFlangerFrequency";
            this.trFlangerFrequency.Size = new System.Drawing.Size(85, 13);
            this.trFlangerFrequency.TabIndex = 1;
            // 
            // trFlangerDelay
            // 
            this.trFlangerDelay.AutoSize = false;
            this.trFlangerDelay.Location = new System.Drawing.Point(71, 143);
            this.trFlangerDelay.Maximum = 90;
            this.trFlangerDelay.Minimum = 1;
            this.trFlangerDelay.Name = "trFlangerDelay";
            this.trFlangerDelay.Size = new System.Drawing.Size(85, 13);
            this.trFlangerDelay.TabIndex = 1;
            this.trFlangerDelay.Value = 1;
            // 
            // TabPage3
            // 
            this.TabPage3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TabPage3.Controls.Add(this.GroupBox5);
            this.TabPage3.Controls.Add(this.GroupBox6);
            this.TabPage3.Location = new System.Drawing.Point(4, 22);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Size = new System.Drawing.Size(544, 252);
            this.TabPage3.TabIndex = 2;
            this.TabPage3.Text = "Crosffade";
            // 
            // GroupBox5
            // 
            this.GroupBox5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.GroupBox5.Controls.Add(this.tbStart);
            this.GroupBox5.Controls.Add(this.Label29);
            this.GroupBox5.Controls.Add(this.lblStart);
            this.GroupBox5.Location = new System.Drawing.Point(7, 154);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(530, 95);
            this.GroupBox5.TabIndex = 6;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "Crossfade start time (time before end of song)";
            // 
            // tbStart
            // 
            this.tbStart.AutoSize = false;
            this.tbStart.Location = new System.Drawing.Point(111, 37);
            this.tbStart.Maximum = 30;
            this.tbStart.Name = "tbStart";
            this.tbStart.Size = new System.Drawing.Size(310, 38);
            this.tbStart.TabIndex = 0;
            this.tbStart.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbStart.Value = 10;
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Location = new System.Drawing.Point(34, 55);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(47, 13);
            this.Label29.TabIndex = 1;
            this.Label29.Text = "Duration";
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(427, 55);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(39, 13);
            this.lblStart.TabIndex = 1;
            this.lblStart.Text = "10 sec";
            // 
            // GroupBox6
            // 
            this.GroupBox6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.GroupBox6.Controls.Add(this.lblOUT);
            this.GroupBox6.Controls.Add(this.Label30);
            this.GroupBox6.Controls.Add(this.tbOut);
            this.GroupBox6.Controls.Add(this.lblIN);
            this.GroupBox6.Controls.Add(this.Label31);
            this.GroupBox6.Controls.Add(this.tbIn);
            this.GroupBox6.Location = new System.Drawing.Point(7, 14);
            this.GroupBox6.Name = "GroupBox6";
            this.GroupBox6.Size = new System.Drawing.Size(530, 134);
            this.GroupBox6.TabIndex = 5;
            this.GroupBox6.TabStop = false;
            this.GroupBox6.Text = "Crossfade duration";
            // 
            // lblOUT
            // 
            this.lblOUT.AutoSize = true;
            this.lblOUT.Location = new System.Drawing.Point(429, 87);
            this.lblOUT.Name = "lblOUT";
            this.lblOUT.Size = new System.Drawing.Size(33, 13);
            this.lblOUT.TabIndex = 1;
            this.lblOUT.Text = "5 sec";
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Location = new System.Drawing.Point(34, 33);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(61, 13);
            this.Label30.TabIndex = 1;
            this.Label30.Text = "IN Duration";
            // 
            // tbOut
            // 
            this.tbOut.AutoSize = false;
            this.tbOut.Location = new System.Drawing.Point(113, 76);
            this.tbOut.Maximum = 30;
            this.tbOut.Minimum = 1;
            this.tbOut.Name = "tbOut";
            this.tbOut.Size = new System.Drawing.Size(310, 38);
            this.tbOut.TabIndex = 0;
            this.tbOut.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbOut.Value = 5;
            // 
            // lblIN
            // 
            this.lblIN.AutoSize = true;
            this.lblIN.Location = new System.Drawing.Point(429, 33);
            this.lblIN.Name = "lblIN";
            this.lblIN.Size = new System.Drawing.Size(33, 13);
            this.lblIN.TabIndex = 1;
            this.lblIN.Text = "5 sec";
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Location = new System.Drawing.Point(36, 87);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(71, 13);
            this.Label31.TabIndex = 1;
            this.Label31.Text = "OUT duration";
            // 
            // tbIn
            // 
            this.tbIn.AutoSize = false;
            this.tbIn.Location = new System.Drawing.Point(113, 19);
            this.tbIn.Maximum = 30;
            this.tbIn.Minimum = 1;
            this.tbIn.Name = "tbIn";
            this.tbIn.Size = new System.Drawing.Size(310, 38);
            this.tbIn.TabIndex = 0;
            this.tbIn.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbIn.Value = 5;
            // 
            // TabPage4
            // 
            this.TabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.TabPage4.Controls.Add(this.gbBalance);
            this.TabPage4.Controls.Add(this.trBalance);
            this.TabPage4.Controls.Add(this.PictureBox1);
            this.TabPage4.Location = new System.Drawing.Point(4, 22);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage4.Size = new System.Drawing.Size(544, 252);
            this.TabPage4.TabIndex = 3;
            this.TabPage4.Text = "Balance";
            // 
            // gbBalance
            // 
            this.gbBalance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbBalance.Location = new System.Drawing.Point(163, 52);
            this.gbBalance.Name = "gbBalance";
            this.gbBalance.Size = new System.Drawing.Size(212, 23);
            this.gbBalance.TabIndex = 10;
            this.gbBalance.Text = "Balance 0";
            this.gbBalance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trBalance
            // 
            this.trBalance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.trBalance.Location = new System.Drawing.Point(78, 93);
            this.trBalance.Maximum = 100;
            this.trBalance.Minimum = -100;
            this.trBalance.Name = "trBalance";
            this.trBalance.Size = new System.Drawing.Size(384, 45);
            this.trBalance.TabIndex = 8;
            this.trBalance.TickFrequency = 5;
            this.trBalance.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // TabPage5
            // 
            this.TabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.TabPage5.Controls.Add(this.trSystemVolume);
            this.TabPage5.Location = new System.Drawing.Point(4, 22);
            this.TabPage5.Name = "TabPage5";
            this.TabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage5.Size = new System.Drawing.Size(544, 252);
            this.TabPage5.TabIndex = 4;
            this.TabPage5.Text = "System Volume";
            // 
            // trSystemVolume
            // 
            this.trSystemVolume.Location = new System.Drawing.Point(26, 28);
            this.trSystemVolume.Name = "trSystemVolume";
            this.trSystemVolume.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trSystemVolume.Size = new System.Drawing.Size(45, 141);
            this.trSystemVolume.TabIndex = 0;
            this.trSystemVolume.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PictureBox1.BackgroundImage")));
            this.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PictureBox1.Location = new System.Drawing.Point(262, 139);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(16, 16);
            this.PictureBox1.TabIndex = 9;
            this.PictureBox1.TabStop = false;
            // 
            // frmEQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 223);
            this.Controls.Add(this.TabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmEQ";
            this.ShowInTaskbar = false;
            this.Text = "frmEQ";
            this.Load += new System.EventHandler(this.frmEQ_Load);
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EQBand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ9)).EndInit();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trChorus)).EndInit();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbReverbMix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbInGain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbReverbTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trReverbHighFreqRTRatio)).EndInit();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoRightDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoLeftDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoWetDry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trEchoFeedback)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerFeedback)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerWetDry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trFlangerDelay)).EndInit();
            this.TabPage3.ResumeLayout(false);
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbStart)).EndInit();
            this.GroupBox6.ResumeLayout(false);
            this.GroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbIn)).EndInit();
            this.TabPage4.ResumeLayout(false);
            this.TabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trBalance)).EndInit();
            this.TabPage5.ResumeLayout(false);
            this.TabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trSystemVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Button btnRemovePreset;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Button btnSavePreset;
        internal System.Windows.Forms.ComboBox cbPresets;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TrackBar EQBand;
        internal System.Windows.Forms.TrackBar EQ0;
        internal System.Windows.Forms.TrackBar EQ1;
        internal System.Windows.Forms.TrackBar EQ2;
        internal System.Windows.Forms.TrackBar EQ3;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TrackBar EQ4;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TrackBar EQ5;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TrackBar EQ6;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.TrackBar EQ7;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.TrackBar EQ8;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.TrackBar EQ9;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.TrackBar trChorus;
        internal System.Windows.Forms.Button btnKillBass;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.Button btnReverb;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.TrackBar trReverbReverbMix;
        internal System.Windows.Forms.TrackBar trReverbInGain;
        internal System.Windows.Forms.TrackBar trReverbReverbTime;
        internal System.Windows.Forms.TrackBar trReverbHighFreqRTRatio;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Button btnEcho;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TrackBar trEchoRightDelay;
        internal System.Windows.Forms.TrackBar trEchoLeftDelay;
        internal System.Windows.Forms.TrackBar trEchoWetDry;
        internal System.Windows.Forms.TrackBar trEchoFeedback;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button btnFlanger;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TrackBar trFlangerFeedback;
        internal System.Windows.Forms.TrackBar trFlangerWetDry;
        internal System.Windows.Forms.TrackBar trFlangerDepth;
        internal System.Windows.Forms.TrackBar trFlangerFrequency;
        internal System.Windows.Forms.TrackBar trFlangerDelay;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.TrackBar tbStart;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label lblStart;
        internal System.Windows.Forms.GroupBox GroupBox6;
        internal System.Windows.Forms.Label lblOUT;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.TrackBar tbOut;
        internal System.Windows.Forms.Label lblIN;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.TrackBar tbIn;
        internal System.Windows.Forms.TabPage TabPage4;
        internal System.Windows.Forms.Label gbBalance;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TrackBar trBalance;
        internal System.Windows.Forms.TabPage TabPage5;
        internal System.Windows.Forms.TrackBar trSystemVolume;
    }
}