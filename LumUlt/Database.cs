﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.SqlServerCe;

namespace LumUlt
{
    class Database
    {


        static Database() 
        {
            ConnString = @"Data Source='lmdb.sdf'";
            EF = new lmdbEntities();
            Conn = new SqlCeConnection(ConnString);
            Table = Func.CreateEmptyDataTable(typeof(tblFiles));
            PrmKey = dbPrmKey.Filename;
        }
        //private static lmdbContext dbEntity = new lmdbContext();
        public static string ConnString {get;set;}
        public static SqlCeConnection Conn { get; set; }
        public static DataTable Table { get; set; }
        static lmdbEntities EF;
        public enum dbPrmKey{Url=1,Filename=2};
        public static dbPrmKey PrmKey;
        public static void AddFile(tblFiles file)
        {
            tblFiles f= GetFile(file);
            if (f == null) { 
                file.DateAdded = DateTime.Now;
                file.DateListened = DateTime.Now;
                file.Disc = "";
                EF.tblFiles.Add(file);
                EF.SaveChanges();
            }
        }


        //string insert = "INSERT INTO tblFiles SET ";
        //foreach (System.Reflection.PropertyInfo pi in file.GetType().GetProperties())
        //{
        //    insert += pi.Name + "=" + file.GetType().GetProperty(pi.Name).GetValue(null, null).ToString();
        //}
        //MessageBox.Show(insert);
        public static void SaveChanges()
        {
            EF.SaveChanges();
        }

        public static List<tblTags> GetTags()
        {
            return  EF.tblTags.ToList();
        }

        public static void AddRemoveTag(string tag, string url)
        {
            tblFiles f = GetFile(url);
            if (f != null)
            {


            }
        }

        public static tblFiles GetFile(string url)
        {
            tblFiles f=null;
            switch (PrmKey)
            {
                case dbPrmKey.Filename:
                    string fName = System.IO.Path.GetFileName(url);
                    f = EF.tblFiles.Where(u => u.Filename == fName).FirstOrDefault();
                    break;                
                case dbPrmKey.Url:
                    f = EF.tblFiles.Where(u => u.Url == url).FirstOrDefault();
                    break;
            }

           return f;
        }

        public static tblFiles GetFile(tblFiles file)
        {
                    return GetFile(file.Url);    
        }

        //public static void AddFile(tblFiles file)
        //{
        //    return;
        //    if (!dbEntity.tblFiles.Any(f => f.Filepath == file.Filepath)) 
        //    {
        //        file.DateAdded = DateTime.Now;
        //        dbEntity.tblFiles.Add(file);
        //        dbEntity.SaveChanges();
        //    }

        //}
        //public static void AddFile(string path)
        //{

        //    tblFiles file = dbEntity.tblFiles.Where(f => f.Filepath == path).FirstOrDefault();
        //    if (file == null)
        //    {
        //        file = Files.GetFullInfo(path);
        //        file.DateAdded = DateTime.Now;
        //        dbEntity.tblFiles.Add(file);
        //        dbEntity.SaveChanges();
        //    }
            
            
        //    //try { e.SaveChanges(); }

        //    //catch (DbEntityValidationException dbEx)
        //    //{
        //    //    foreach (var validationErrors in dbEx.EntityValidationErrors)
        //    //    {
        //    //        foreach (var validationError in validationErrors.ValidationErrors)
        //    //        {
        //    //            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);

        //    //            MessageBox.Show(validationError.PropertyName);
        //    //            MessageBox.Show(validationError.ErrorMessage);
        //    //        }
        //    //    }
        //    //}
            

        //}

        public static DataTable Search(string searchText)
        {
            try
            {
                Table.Rows.Clear();
                //SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM tblFiles", Conn);
                
                SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM tblFiles WHERE FilePath Like @pitem", Conn);
                cmd.Parameters.AddWithValue("pitem", "%" + searchText + "%");
                SqlCeDataAdapter da = new SqlCeDataAdapter(cmd);
                da.Fill(Table);
                return Table;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); 
                return Table; 
            }
  
        }


    }

    

}
