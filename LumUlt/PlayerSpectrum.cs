﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Un4seen.Bass;
using Un4seen.Bass.Misc;
using System.Drawing;
using System.Drawing.Imaging;

namespace LumUlt
{
    public static partial class Player
    {
        public enum SpectrumType
        {Default=0,Elipse=1,Dot=2,Wave=3,Bean=4,LinePeak=5,WaveForm=6}

        public static Color SpectrumColor1 { get; set; }
        public static Color SpectrumColor2 { get; set; }
        public static Color SpectrumColor3 { get; set; }
        public static Color SpectrumBackgroundColor { get; set; }
        public static int SpectrumLineWidth { get; set; }
        public static int SpectrumLineDistance { get; set; }
        public static bool SpectrumHighQuality { get; set; }
        public static SpectrumType SpectrumMode { get; set; }
        public static Bitmap GetSpectrum(Size size)
        {
            //vraca null ako nista ne svira 
            if (State == PlayerState.Playing | State == PlayerState.Streaming | State == PlayerState.Paused)
            {
                Bitmap bmp = new Bitmap(size.Width, size.Height);
                Visuals vis = new Visuals();
                bool SpectrumLinear = true;
                bool SpectrumFullSpectrum = false;
                switch (SpectrumMode)
                {
                    case SpectrumType.WaveForm:
                        return vis.CreateWaveForm(CurrentChannel, size.Width, size.Height,
                        SpectrumColor1, SpectrumColor2, SpectrumColor3, SpectrumBackgroundColor, SpectrumLineWidth
                        , true, false, SpectrumHighQuality);

                    case SpectrumType.Elipse:
                        return vis.CreateSpectrumEllipse(CurrentChannel, size.Width, size.Height,
                        SpectrumColor1, SpectrumColor2, SpectrumBackgroundColor, SpectrumLineWidth
                        , SpectrumLineDistance, SpectrumLinear, SpectrumFullSpectrum, SpectrumHighQuality);

                    case SpectrumType.Wave:
                        return vis.CreateSpectrumWave(CurrentChannel, size.Width, size.Height,
                        SpectrumColor1, SpectrumColor2, SpectrumBackgroundColor, SpectrumLineWidth
                        , SpectrumLinear, SpectrumFullSpectrum, SpectrumHighQuality);

                    case SpectrumType.Bean:
                        return vis.CreateSpectrumBean(CurrentChannel, size.Width, size.Height,
                        SpectrumColor1, SpectrumColor2, SpectrumBackgroundColor, SpectrumLineWidth
                        , SpectrumLinear, SpectrumFullSpectrum, SpectrumHighQuality);

                    case SpectrumType.Dot:
                        return vis.CreateSpectrumDot(CurrentChannel, size.Width, size.Height,
                        SpectrumColor1, SpectrumColor2, SpectrumBackgroundColor, SpectrumLineWidth
                        , SpectrumLineDistance, SpectrumLinear, SpectrumFullSpectrum, SpectrumHighQuality);

                    case SpectrumType.LinePeak:
                        return vis.CreateSpectrumLinePeak(CurrentChannel, size.Width, size.Height,
                        SpectrumColor1, SpectrumColor2, SpectrumColor3, SpectrumBackgroundColor, SpectrumLineWidth
                        , SpectrumLineWidth, SpectrumLineDistance, 1000, SpectrumLinear, SpectrumFullSpectrum, SpectrumHighQuality);

                    default:
                        return vis.CreateSpectrumLine(CurrentChannel, size.Width, size.Height,
                        SpectrumColor1, SpectrumColor2, SpectrumBackgroundColor, SpectrumLineWidth
                        , SpectrumLineDistance, SpectrumLinear, SpectrumFullSpectrum, SpectrumHighQuality);
                }
            }
            return null; 
        }


      
        public static void NextViz()
        {
            var intModeCount = Enum.GetNames(typeof(SpectrumType)).Length;
            int eValue = (int)SpectrumMode;
            eValue += 1;
            if (eValue == intModeCount) eValue = 0;
            SpectrumMode = (SpectrumType)eValue;

        }
        

    }
}
