﻿namespace LumUlt
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tv0 = new System.Windows.Forms.TreeView();
            this.IM1 = new System.Windows.Forms.ImageList(this.components);
            this.tsFiles = new System.Windows.Forms.ToolStrip();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.btnFavorites = new System.Windows.Forms.ToolStripDropDownButton();
            this.tv1 = new System.Windows.Forms.TreeView();
            this.dgFiles = new System.Windows.Forms.DataGridView();
            this.tsExplorer = new System.Windows.Forms.ToolStrip();
            this.btnExplorer = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSearch = new System.Windows.Forms.ToolStripSplitButton();
            this.searchYoutubeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchGoogleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.btnClearSearch = new System.Windows.Forms.ToolStripButton();
            this.SearchProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.lblSearchFolder = new System.Windows.Forms.ToolStripLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tVis = new System.Windows.Forms.Timer(this.components);
            this.cmDatabase = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsDatabaseProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.cmFolder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToFavoritesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmFiles = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fILESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dJSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sc0 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.TagStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.lstEQPresets = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.EQ0 = new System.Windows.Forms.TrackBar();
            this.EQ9 = new System.Windows.Forms.TrackBar();
            this.EQ8 = new System.Windows.Forms.TrackBar();
            this.EQ1 = new System.Windows.Forms.TrackBar();
            this.EQ7 = new System.Windows.Forms.TrackBar();
            this.EQ2 = new System.Windows.Forms.TrackBar();
            this.EQ6 = new System.Windows.Forms.TrackBar();
            this.EQ3 = new System.Windows.Forms.TrackBar();
            this.EQ5 = new System.Windows.Forms.TrackBar();
            this.EQ4 = new System.Windows.Forms.TrackBar();
            this.EQBand = new System.Windows.Forms.TrackBar();
            this.button12 = new System.Windows.Forms.Button();
            this.btnSavePreset = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.progress = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.trVol = new System.Windows.Forms.TrackBar();
            this.btnPlayerEQ = new System.Windows.Forms.Button();
            this.btnPlayerTag = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnPlay = new System.Windows.Forms.Button();
            this.lblDuration = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblTime = new System.Windows.Forms.Label();
            this.PicViz = new System.Windows.Forms.PictureBox();
            this.deepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sumerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tranceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.favoriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.houseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.technoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tsFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFiles)).BeginInit();
            this.tsExplorer.SuspendLayout();
            this.cmDatabase.SuspendLayout();
            this.cmFolder.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sc0)).BeginInit();
            this.sc0.Panel1.SuspendLayout();
            this.sc0.Panel2.SuspendLayout();
            this.sc0.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.TagStrip.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EQ0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQBand)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trVol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicViz)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tv0);
            this.splitContainer1.Panel1.Controls.Add(this.tsFiles);
            this.splitContainer1.Panel1.Controls.Add(this.tv1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgFiles);
            this.splitContainer1.Panel2.Controls.Add(this.tsExplorer);
            this.splitContainer1.Size = new System.Drawing.Size(777, 296);
            this.splitContainer1.SplitterDistance = 178;
            this.splitContainer1.TabIndex = 1;
            // 
            // tv0
            // 
            this.tv0.AllowDrop = true;
            this.tv0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tv0.BackColor = System.Drawing.SystemColors.Control;
            this.tv0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tv0.FullRowSelect = true;
            this.tv0.HideSelection = false;
            this.tv0.ImageIndex = 0;
            this.tv0.ImageList = this.IM1;
            this.tv0.Location = new System.Drawing.Point(3, 25);
            this.tv0.Name = "tv0";
            this.tv0.SelectedImageIndex = 0;
            this.tv0.ShowLines = false;
            this.tv0.ShowPlusMinus = false;
            this.tv0.ShowRootLines = false;
            this.tv0.Size = new System.Drawing.Size(172, 72);
            this.tv0.TabIndex = 1;
            this.tv0.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv0_AfterSelect);
            // 
            // IM1
            // 
            this.IM1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IM1.ImageStream")));
            this.IM1.TransparentColor = System.Drawing.Color.Transparent;
            this.IM1.Images.SetKeyName(0, "folder.png");
            this.IM1.Images.SetKeyName(1, "foldersub.png");
            this.IM1.Images.SetKeyName(2, "dot.png");
            this.IM1.Images.SetKeyName(3, "dot.png");
            this.IM1.Images.SetKeyName(4, "dot.png");
            // 
            // tsFiles
            // 
            this.tsFiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton1,
            this.btnFavorites});
            this.tsFiles.Location = new System.Drawing.Point(0, 0);
            this.tsFiles.Name = "tsFiles";
            this.tsFiles.Size = new System.Drawing.Size(178, 25);
            this.tsFiles.TabIndex = 0;
            this.tsFiles.Text = "toolStrip2";
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(32, 22);
            this.toolStripSplitButton1.Text = "toolStripSplitButton1";
            // 
            // btnFavorites
            // 
            this.btnFavorites.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFavorites.Image = ((System.Drawing.Image)(resources.GetObject("btnFavorites.Image")));
            this.btnFavorites.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFavorites.Name = "btnFavorites";
            this.btnFavorites.Size = new System.Drawing.Size(29, 22);
            this.btnFavorites.Text = "toolStripDropDownButton1";
            this.btnFavorites.Click += new System.EventHandler(this.btnFavorites_Click);
            // 
            // tv1
            // 
            this.tv1.AllowDrop = true;
            this.tv1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tv1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tv1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tv1.FullRowSelect = true;
            this.tv1.ImageIndex = 0;
            this.tv1.ImageList = this.IM1;
            this.tv1.Location = new System.Drawing.Point(3, 98);
            this.tv1.Name = "tv1";
            this.tv1.SelectedImageIndex = 0;
            this.tv1.ShowLines = false;
            this.tv1.ShowPlusMinus = false;
            this.tv1.ShowRootLines = false;
            this.tv1.Size = new System.Drawing.Size(172, 198);
            this.tv1.TabIndex = 1;
            this.tv1.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.tv1_BeforeExpand);
            this.tv1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv1_AfterSelect);
            this.tv1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tv1_NodeMouseClick);
            this.tv1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tv1_NodeMouseDoubleClick);
            this.tv1.DragDrop += new System.Windows.Forms.DragEventHandler(this.tv1_DragDrop);
            this.tv1.DragOver += new System.Windows.Forms.DragEventHandler(this.tv1_DragOver);
            this.tv1.DragLeave += new System.EventHandler(this.tv1_DragLeave);
            this.tv1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tv1_MouseUp);
            // 
            // dgFiles
            // 
            this.dgFiles.AllowUserToAddRows = false;
            this.dgFiles.AllowUserToDeleteRows = false;
            this.dgFiles.AllowUserToOrderColumns = true;
            this.dgFiles.AllowUserToResizeRows = false;
            this.dgFiles.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.dgFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgFiles.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgFiles.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgFiles.GridColor = System.Drawing.Color.SteelBlue;
            this.dgFiles.Location = new System.Drawing.Point(0, 25);
            this.dgFiles.Name = "dgFiles";
            this.dgFiles.RowHeadersVisible = false;
            this.dgFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFiles.Size = new System.Drawing.Size(595, 271);
            this.dgFiles.TabIndex = 1;
            this.dgFiles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFiles_CellContentClick);
            this.dgFiles.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgFiles_CellMouseDown);
            this.dgFiles.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgFiles_ColumnWidthChanged);
            this.dgFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgFiles_MouseDoubleClick);
            this.dgFiles.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgFiles_MouseDown);
            this.dgFiles.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgFiles_MouseMove);
            this.dgFiles.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgFiles_MouseUp);
            // 
            // tsExplorer
            // 
            this.tsExplorer.CanOverflow = false;
            this.tsExplorer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExplorer,
            this.toolStripSeparator1,
            this.btnSearch,
            this.txtSearch,
            this.btnClearSearch,
            this.SearchProgress,
            this.lblSearchFolder});
            this.tsExplorer.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsExplorer.Location = new System.Drawing.Point(0, 0);
            this.tsExplorer.Name = "tsExplorer";
            this.tsExplorer.Size = new System.Drawing.Size(595, 25);
            this.tsExplorer.TabIndex = 0;
            this.tsExplorer.Text = "toolStrip1";
            // 
            // btnExplorer
            // 
            this.btnExplorer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnExplorer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnExplorer.ForeColor = System.Drawing.Color.DimGray;
            this.btnExplorer.Image = ((System.Drawing.Image)(resources.GetObject("btnExplorer.Image")));
            this.btnExplorer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExplorer.Name = "btnExplorer";
            this.btnExplorer.Size = new System.Drawing.Size(36, 22);
            this.btnExplorer.Text = "Zika";
            this.btnExplorer.Click += new System.EventHandler(this.btnExplorer_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnSearch
            // 
            this.btnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSearch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchYoutubeToolStripMenuItem,
            this.searchGoogleToolStripMenuItem});
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(32, 22);
            this.btnSearch.Text = "Search";
            this.btnSearch.ButtonClick += new System.EventHandler(this.btnSearch_ButtonClick);
            // 
            // searchYoutubeToolStripMenuItem
            // 
            this.searchYoutubeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchYoutubeToolStripMenuItem.Image")));
            this.searchYoutubeToolStripMenuItem.Name = "searchYoutubeToolStripMenuItem";
            this.searchYoutubeToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.searchYoutubeToolStripMenuItem.Text = "Search Youtube";
            // 
            // searchGoogleToolStripMenuItem
            // 
            this.searchGoogleToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchGoogleToolStripMenuItem.Image")));
            this.searchGoogleToolStripMenuItem.Name = "searchGoogleToolStripMenuItem";
            this.searchGoogleToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.searchGoogleToolStripMenuItem.Text = "Search Google for mp3";
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtSearch.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(150, 25);
            this.txtSearch.Click += new System.EventHandler(this.txtSearch_Click);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnClearSearch
            // 
            this.btnClearSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClearSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnClearSearch.Image")));
            this.btnClearSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClearSearch.Name = "btnClearSearch";
            this.btnClearSearch.Size = new System.Drawing.Size(23, 22);
            this.btnClearSearch.Text = "toolStripButton1";
            this.btnClearSearch.Click += new System.EventHandler(this.btnClearSearch_Click);
            // 
            // SearchProgress
            // 
            this.SearchProgress.Name = "SearchProgress";
            this.SearchProgress.Size = new System.Drawing.Size(100, 22);
            this.SearchProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.SearchProgress.Visible = false;
            // 
            // lblSearchFolder
            // 
            this.lblSearchFolder.AutoSize = false;
            this.lblSearchFolder.Name = "lblSearchFolder";
            this.lblSearchFolder.Size = new System.Drawing.Size(300, 22);
            this.lblSearchFolder.Text = "Loop";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tVis
            // 
            this.tVis.Interval = 10;
            this.tVis.Tick += new System.EventHandler(this.tVis_Tick);
            // 
            // cmDatabase
            // 
            this.cmDatabase.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDatabaseProperties});
            this.cmDatabase.Name = "cmDatabase";
            this.cmDatabase.Size = new System.Drawing.Size(124, 26);
            // 
            // tsDatabaseProperties
            // 
            this.tsDatabaseProperties.Name = "tsDatabaseProperties";
            this.tsDatabaseProperties.Size = new System.Drawing.Size(123, 22);
            this.tsDatabaseProperties.Text = "Properties";
            this.tsDatabaseProperties.Click += new System.EventHandler(this.tsDatabaseProperties_Click);
            // 
            // cmFolder
            // 
            this.cmFolder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.propertiesToolStripMenuItem,
            this.addToFavoritesToolStripMenuItem});
            this.cmFolder.Name = "contextMenuStrip1";
            this.cmFolder.Size = new System.Drawing.Size(156, 48);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.propertiesToolStripMenuItem.Text = "Properties";
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // addToFavoritesToolStripMenuItem
            // 
            this.addToFavoritesToolStripMenuItem.Name = "addToFavoritesToolStripMenuItem";
            this.addToFavoritesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.addToFavoritesToolStripMenuItem.Text = "Add to  favorites";
            this.addToFavoritesToolStripMenuItem.Click += new System.EventHandler(this.addToFavoritesToolStripMenuItem_Click);
            // 
            // cmFiles
            // 
            this.cmFiles.Name = "contextMenuStrip1";
            this.cmFiles.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILESToolStripMenuItem,
            this.selectedFilesToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.dJSToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(777, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fILESToolStripMenuItem
            // 
            this.fILESToolStripMenuItem.Name = "fILESToolStripMenuItem";
            this.fILESToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fILESToolStripMenuItem.Text = "File";
            // 
            // selectedFilesToolStripMenuItem
            // 
            this.selectedFilesToolStripMenuItem.Name = "selectedFilesToolStripMenuItem";
            this.selectedFilesToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.selectedFilesToolStripMenuItem.Text = "Selected files";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // dJSToolStripMenuItem
            // 
            this.dJSToolStripMenuItem.Name = "dJSToolStripMenuItem";
            this.dJSToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.dJSToolStripMenuItem.Text = "DJ\'S";
            // 
            // sc0
            // 
            this.sc0.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sc0.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.sc0.IsSplitterFixed = true;
            this.sc0.Location = new System.Drawing.Point(0, 24);
            this.sc0.Name = "sc0";
            this.sc0.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // sc0.Panel1
            // 
            this.sc0.Panel1.Controls.Add(this.splitContainer1);
            // 
            // sc0.Panel2
            // 
            this.sc0.Panel2.Controls.Add(this.tabControl1);
            this.sc0.Size = new System.Drawing.Size(777, 418);
            this.sc0.SplitterDistance = 296;
            this.sc0.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(777, 118);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.TagStrip);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(769, 89);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Tag = "IC";
            this.tabPage2.Text = "Tags";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // TagStrip
            // 
            this.TagStrip.AutoSize = false;
            this.TagStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TagStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.TagStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.TagStrip.Location = new System.Drawing.Point(3, 3);
            this.TagStrip.Name = "TagStrip";
            this.TagStrip.Size = new System.Drawing.Size(763, 83);
            this.TagStrip.TabIndex = 0;
            this.TagStrip.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.BackColor = System.Drawing.Color.Gray;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(50, 17);
            this.toolStripButton1.Text = "htzzthzt";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.lstEQPresets);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.EQBand);
            this.tabPage1.Controls.Add(this.button12);
            this.tabPage1.Controls.Add(this.btnSavePreset);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(142, 17);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Tag = "XC";
            this.tabPage1.Text = "EQ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Presets";
            // 
            // lstEQPresets
            // 
            this.lstEQPresets.BackColor = System.Drawing.SystemColors.Control;
            this.lstEQPresets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstEQPresets.ForeColor = System.Drawing.Color.Gray;
            this.lstEQPresets.FormattingEnabled = true;
            this.lstEQPresets.Items.AddRange(new object[] {
            "Headphones",
            "Speakers",
            "Kill Bass"});
            this.lstEQPresets.Location = new System.Drawing.Point(67, 8);
            this.lstEQPresets.Name = "lstEQPresets";
            this.lstEQPresets.Size = new System.Drawing.Size(109, 65);
            this.lstEQPresets.TabIndex = 14;
            this.lstEQPresets.SelectedIndexChanged += new System.EventHandler(this.lstEQPresets_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.Controls.Add(this.EQ0);
            this.panel2.Controls.Add(this.EQ9);
            this.panel2.Controls.Add(this.EQ8);
            this.panel2.Controls.Add(this.EQ1);
            this.panel2.Controls.Add(this.EQ7);
            this.panel2.Controls.Add(this.EQ2);
            this.panel2.Controls.Add(this.EQ6);
            this.panel2.Controls.Add(this.EQ3);
            this.panel2.Controls.Add(this.EQ5);
            this.panel2.Controls.Add(this.EQ4);
            this.panel2.Location = new System.Drawing.Point(-123, -31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(422, 77);
            this.panel2.TabIndex = 13;
            // 
            // EQ0
            // 
            this.EQ0.BackColor = System.Drawing.SystemColors.Control;
            this.EQ0.Location = new System.Drawing.Point(3, 1);
            this.EQ0.Maximum = 15;
            this.EQ0.Minimum = -15;
            this.EQ0.Name = "EQ0";
            this.EQ0.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ0.Size = new System.Drawing.Size(42, 78);
            this.EQ0.TabIndex = 3;
            this.EQ0.Tag = "0";
            this.EQ0.TickFrequency = 3;
            this.EQ0.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ0.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ9
            // 
            this.EQ9.BackColor = System.Drawing.SystemColors.Control;
            this.EQ9.Location = new System.Drawing.Point(370, 1);
            this.EQ9.Maximum = 15;
            this.EQ9.Minimum = -15;
            this.EQ9.Name = "EQ9";
            this.EQ9.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ9.Size = new System.Drawing.Size(42, 78);
            this.EQ9.TabIndex = 12;
            this.EQ9.Tag = "9";
            this.EQ9.TickFrequency = 3;
            this.EQ9.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ9.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ8
            // 
            this.EQ8.BackColor = System.Drawing.SystemColors.Control;
            this.EQ8.Location = new System.Drawing.Point(328, 1);
            this.EQ8.Maximum = 15;
            this.EQ8.Minimum = -15;
            this.EQ8.Name = "EQ8";
            this.EQ8.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ8.Size = new System.Drawing.Size(42, 78);
            this.EQ8.TabIndex = 11;
            this.EQ8.Tag = "8";
            this.EQ8.TickFrequency = 3;
            this.EQ8.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ8.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ1
            // 
            this.EQ1.BackColor = System.Drawing.SystemColors.Control;
            this.EQ1.Location = new System.Drawing.Point(45, 1);
            this.EQ1.Maximum = 15;
            this.EQ1.Minimum = -15;
            this.EQ1.Name = "EQ1";
            this.EQ1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ1.Size = new System.Drawing.Size(42, 78);
            this.EQ1.TabIndex = 4;
            this.EQ1.Tag = "1";
            this.EQ1.TickFrequency = 3;
            this.EQ1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ1.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ7
            // 
            this.EQ7.BackColor = System.Drawing.SystemColors.Control;
            this.EQ7.Location = new System.Drawing.Point(286, 1);
            this.EQ7.Maximum = 15;
            this.EQ7.Minimum = -15;
            this.EQ7.Name = "EQ7";
            this.EQ7.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ7.Size = new System.Drawing.Size(42, 78);
            this.EQ7.TabIndex = 10;
            this.EQ7.Tag = "7";
            this.EQ7.TickFrequency = 3;
            this.EQ7.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ7.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ2
            // 
            this.EQ2.BackColor = System.Drawing.SystemColors.Control;
            this.EQ2.Location = new System.Drawing.Point(87, 1);
            this.EQ2.Maximum = 15;
            this.EQ2.Minimum = -15;
            this.EQ2.Name = "EQ2";
            this.EQ2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ2.Size = new System.Drawing.Size(42, 78);
            this.EQ2.TabIndex = 5;
            this.EQ2.Tag = "2";
            this.EQ2.TickFrequency = 3;
            this.EQ2.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ2.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ6
            // 
            this.EQ6.BackColor = System.Drawing.SystemColors.Control;
            this.EQ6.Location = new System.Drawing.Point(244, 1);
            this.EQ6.Maximum = 15;
            this.EQ6.Minimum = -15;
            this.EQ6.Name = "EQ6";
            this.EQ6.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ6.Size = new System.Drawing.Size(42, 78);
            this.EQ6.TabIndex = 9;
            this.EQ6.Tag = "6";
            this.EQ6.TickFrequency = 3;
            this.EQ6.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ6.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ3
            // 
            this.EQ3.BackColor = System.Drawing.SystemColors.Control;
            this.EQ3.Location = new System.Drawing.Point(126, 1);
            this.EQ3.Maximum = 15;
            this.EQ3.Minimum = -15;
            this.EQ3.Name = "EQ3";
            this.EQ3.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ3.Size = new System.Drawing.Size(42, 78);
            this.EQ3.TabIndex = 6;
            this.EQ3.Tag = "3";
            this.EQ3.TickFrequency = 3;
            this.EQ3.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ3.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ5
            // 
            this.EQ5.BackColor = System.Drawing.SystemColors.Control;
            this.EQ5.Location = new System.Drawing.Point(202, 1);
            this.EQ5.Maximum = 15;
            this.EQ5.Minimum = -15;
            this.EQ5.Name = "EQ5";
            this.EQ5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ5.Size = new System.Drawing.Size(42, 78);
            this.EQ5.TabIndex = 8;
            this.EQ5.Tag = "5";
            this.EQ5.TickFrequency = 3;
            this.EQ5.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ5.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQ4
            // 
            this.EQ4.BackColor = System.Drawing.SystemColors.Control;
            this.EQ4.Location = new System.Drawing.Point(166, 1);
            this.EQ4.Maximum = 15;
            this.EQ4.Minimum = -15;
            this.EQ4.Name = "EQ4";
            this.EQ4.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQ4.Size = new System.Drawing.Size(42, 78);
            this.EQ4.TabIndex = 7;
            this.EQ4.Tag = "4";
            this.EQ4.TickFrequency = 3;
            this.EQ4.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQ4.Scroll += new System.EventHandler(this.EQScroll);
            // 
            // EQBand
            // 
            this.EQBand.BackColor = System.Drawing.SystemColors.Control;
            this.EQBand.Enabled = false;
            this.EQBand.Location = new System.Drawing.Point(719, 3);
            this.EQBand.Maximum = 12;
            this.EQBand.Minimum = -12;
            this.EQBand.Name = "EQBand";
            this.EQBand.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EQBand.Size = new System.Drawing.Size(42, 78);
            this.EQBand.TabIndex = 2;
            this.EQBand.TickFrequency = 3;
            this.EQBand.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.EQBand.Visible = false;
            // 
            // button12
            // 
            this.button12.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Location = new System.Drawing.Point(6, 52);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(55, 21);
            this.button12.TabIndex = 8;
            this.button12.UseVisualStyleBackColor = true;
            // 
            // btnSavePreset
            // 
            this.btnSavePreset.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.btnSavePreset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSavePreset.Location = new System.Drawing.Point(6, 25);
            this.btnSavePreset.Name = "btnSavePreset";
            this.btnSavePreset.Size = new System.Drawing.Size(55, 21);
            this.btnSavePreset.TabIndex = 8;
            this.btnSavePreset.Text = "Save";
            this.btnSavePreset.UseVisualStyleBackColor = true;
            this.btnSavePreset.Click += new System.EventHandler(this.btnSavePreset_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(142, 17);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Now playing";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(142, 17);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Crossfade";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(142, 17);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "DJ";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.progress);
            this.panel1.Controls.Add(this.lblTitle);
            this.panel1.Controls.Add(this.trVol);
            this.panel1.Controls.Add(this.btnPlayerEQ);
            this.panel1.Controls.Add(this.btnPlayerTag);
            this.panel1.Controls.Add(this.btnPrev);
            this.panel1.Controls.Add(this.btnStop);
            this.panel1.Controls.Add(this.lblStatus);
            this.panel1.Controls.Add(this.btnPlay);
            this.panel1.Controls.Add(this.lblDuration);
            this.panel1.Controls.Add(this.btnNext);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.PicViz);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 448);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(777, 47);
            this.panel1.TabIndex = 6;
            // 
            // progress
            // 
            this.progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.progress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.progress.Location = new System.Drawing.Point(162, 13);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(485, 13);
            this.progress.TabIndex = 18;
            this.progress.TabStop = false;
            this.progress.MouseDown += new System.Windows.Forms.MouseEventHandler(this.progress_MouseDown);
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTitle.ForeColor = System.Drawing.Color.DimGray;
            this.lblTitle.Location = new System.Drawing.Point(162, 2);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(451, 13);
            this.lblTitle.TabIndex = 12;
            this.lblTitle.Text = "DJ RLANDO - JAGUAR";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // trVol
            // 
            this.trVol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trVol.Location = new System.Drawing.Point(653, 0);
            this.trVol.Maximum = 100;
            this.trVol.Name = "trVol";
            this.trVol.Size = new System.Drawing.Size(121, 42);
            this.trVol.TabIndex = 13;
            this.trVol.TickFrequency = 20;
            this.trVol.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trVol.Scroll += new System.EventHandler(this.trVol_Scroll);
            // 
            // btnPlayerEQ
            // 
            this.btnPlayerEQ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlayerEQ.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.btnPlayerEQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayerEQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPlayerEQ.Location = new System.Drawing.Point(529, 26);
            this.btnPlayerEQ.Name = "btnPlayerEQ";
            this.btnPlayerEQ.Size = new System.Drawing.Size(39, 18);
            this.btnPlayerEQ.TabIndex = 8;
            this.btnPlayerEQ.Text = "EQ";
            this.btnPlayerEQ.UseVisualStyleBackColor = true;
            // 
            // btnPlayerTag
            // 
            this.btnPlayerTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlayerTag.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.btnPlayerTag.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayerTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPlayerTag.Location = new System.Drawing.Point(488, 26);
            this.btnPlayerTag.Name = "btnPlayerTag";
            this.btnPlayerTag.Size = new System.Drawing.Size(39, 18);
            this.btnPlayerTag.TabIndex = 8;
            this.btnPlayerTag.Text = "TAG";
            this.btnPlayerTag.UseVisualStyleBackColor = true;
            this.btnPlayerTag.Click += new System.EventHandler(this.btnPlayerTag_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.btnPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrev.Image = ((System.Drawing.Image)(resources.GetObject("btnPrev.Image")));
            this.btnPrev.Location = new System.Drawing.Point(4, 3);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(39, 41);
            this.btnPrev.TabIndex = 8;
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.Location = new System.Drawing.Point(80, 3);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(39, 41);
            this.btnStop.TabIndex = 9;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblStatus.ForeColor = System.Drawing.Color.DarkGray;
            this.lblStatus.Location = new System.Drawing.Point(271, 29);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(60, 15);
            this.lblStatus.TabIndex = 17;
            this.lblStatus.Text = "Null";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPlay
            // 
            this.btnPlay.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlay.Image = ((System.Drawing.Image)(resources.GetObject("btnPlay.Image")));
            this.btnPlay.Location = new System.Drawing.Point(42, 3);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(39, 41);
            this.btnPlay.TabIndex = 10;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // lblDuration
            // 
            this.lblDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDuration.AutoSize = true;
            this.lblDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDuration.ForeColor = System.Drawing.Color.DimGray;
            this.lblDuration.Location = new System.Drawing.Point(619, 26);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(28, 12);
            this.lblDuration.TabIndex = 15;
            this.lblDuration.Text = "03:57";
            // 
            // btnNext
            // 
            this.btnNext.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.Location = new System.Drawing.Point(118, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(39, 41);
            this.btnNext.TabIndex = 11;
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblTime
            // 
            this.lblTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTime.ForeColor = System.Drawing.Color.DimGray;
            this.lblTime.Location = new System.Drawing.Point(586, 26);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(28, 12);
            this.lblTime.TabIndex = 16;
            this.lblTime.Text = "00:45";
            // 
            // PicViz
            // 
            this.PicViz.BackColor = System.Drawing.Color.Transparent;
            this.PicViz.Location = new System.Drawing.Point(162, 29);
            this.PicViz.Name = "PicViz";
            this.PicViz.Size = new System.Drawing.Size(103, 14);
            this.PicViz.TabIndex = 14;
            this.PicViz.TabStop = false;
            this.PicViz.Click += new System.EventHandler(this.PicViz_Click);
            // 
            // deepToolStripMenuItem
            // 
            this.deepToolStripMenuItem.Name = "deepToolStripMenuItem";
            this.deepToolStripMenuItem.Size = new System.Drawing.Size(44, 17);
            this.deepToolStripMenuItem.Text = "Deep";
            this.deepToolStripMenuItem.Click += new System.EventHandler(this.PlayerTagClick);
            // 
            // sumerToolStripMenuItem
            // 
            this.sumerToolStripMenuItem.Name = "sumerToolStripMenuItem";
            this.sumerToolStripMenuItem.Size = new System.Drawing.Size(57, 17);
            this.sumerToolStripMenuItem.Text = "Summer";
            // 
            // danceToolStripMenuItem
            // 
            this.danceToolStripMenuItem.Name = "danceToolStripMenuItem";
            this.danceToolStripMenuItem.Size = new System.Drawing.Size(49, 17);
            this.danceToolStripMenuItem.Text = "Dance";
            // 
            // tranceToolStripMenuItem
            // 
            this.tranceToolStripMenuItem.Name = "tranceToolStripMenuItem";
            this.tranceToolStripMenuItem.Size = new System.Drawing.Size(52, 17);
            this.tranceToolStripMenuItem.Text = "Trance";
            // 
            // favoriteToolStripMenuItem
            // 
            this.favoriteToolStripMenuItem.Name = "favoriteToolStripMenuItem";
            this.favoriteToolStripMenuItem.Size = new System.Drawing.Size(59, 17);
            this.favoriteToolStripMenuItem.Text = "Favorite";
            // 
            // radioToolStripMenuItem
            // 
            this.radioToolStripMenuItem.Name = "radioToolStripMenuItem";
            this.radioToolStripMenuItem.Size = new System.Drawing.Size(46, 17);
            this.radioToolStripMenuItem.Text = "Radio";
            // 
            // houseToolStripMenuItem
            // 
            this.houseToolStripMenuItem.Name = "houseToolStripMenuItem";
            this.houseToolStripMenuItem.Size = new System.Drawing.Size(49, 17);
            this.houseToolStripMenuItem.Text = "House";
            // 
            // technoToolStripMenuItem
            // 
            this.technoToolStripMenuItem.Name = "technoToolStripMenuItem";
            this.technoToolStripMenuItem.Size = new System.Drawing.Size(54, 17);
            this.technoToolStripMenuItem.Text = "Techno";
            // 
            // chillToolStripMenuItem
            // 
            this.chillToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chillToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Control;
            this.chillToolStripMenuItem.Name = "chillToolStripMenuItem";
            this.chillToolStripMenuItem.Size = new System.Drawing.Size(38, 17);
            this.chillToolStripMenuItem.Text = "Chill";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 495);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.sc0);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tsFiles.ResumeLayout(false);
            this.tsFiles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFiles)).EndInit();
            this.tsExplorer.ResumeLayout(false);
            this.tsExplorer.PerformLayout();
            this.cmDatabase.ResumeLayout(false);
            this.cmFolder.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.sc0.Panel1.ResumeLayout(false);
            this.sc0.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sc0)).EndInit();
            this.sc0.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.TagStrip.ResumeLayout(false);
            this.TagStrip.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EQ0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQ4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQBand)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trVol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicViz)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tv1;
        private System.Windows.Forms.ToolStrip tsFiles;
        private System.Windows.Forms.ToolStrip tsExplorer;
        private System.Windows.Forms.DataGridView dgFiles;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripTextBox txtSearch;
        private System.Windows.Forms.Timer tVis;
        private System.Windows.Forms.ImageList IM1;
        private System.Windows.Forms.ToolStripDropDownButton btnFavorites;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnExplorer;
        private System.Windows.Forms.ToolStripSplitButton btnSearch;
        private System.Windows.Forms.ToolStripMenuItem searchYoutubeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchGoogleToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnClearSearch;
        private System.Windows.Forms.ContextMenuStrip cmDatabase;
        private System.Windows.Forms.ToolStripMenuItem tsDatabaseProperties;
        private System.Windows.Forms.ToolStripProgressBar SearchProgress;
        private System.Windows.Forms.ToolStripLabel lblSearchFolder;
        private System.Windows.Forms.ContextMenuStrip cmFolder;
        private System.Windows.Forms.ContextMenuStrip cmFiles;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fILESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dJSToolStripMenuItem;
        private System.Windows.Forms.SplitContainer sc0;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.TrackBar EQBand;
        internal System.Windows.Forms.TrackBar EQ0;
        internal System.Windows.Forms.TrackBar EQ1;
        internal System.Windows.Forms.TrackBar EQ2;
        internal System.Windows.Forms.TrackBar EQ3;
        internal System.Windows.Forms.TrackBar EQ4;
        internal System.Windows.Forms.TrackBar EQ5;
        internal System.Windows.Forms.TrackBar EQ6;
        internal System.Windows.Forms.TrackBar EQ7;
        internal System.Windows.Forms.TrackBar EQ8;
        internal System.Windows.Forms.TrackBar EQ9;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox progress;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TrackBar trVol;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Label lblDuration;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.PictureBox PicViz;
        private System.Windows.Forms.Button btnPlayerTag;
        private System.Windows.Forms.Button btnPlayerEQ;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lstEQPresets;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button btnSavePreset;
        private System.Windows.Forms.ToolStripMenuItem addToFavoritesToolStripMenuItem;
        
        private System.Windows.Forms.ToolStripMenuItem deepToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sumerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tranceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem favoriteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem radioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem houseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem technoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chillToolStripMenuItem;
        private System.Windows.Forms.ToolStrip TagStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.TreeView tv0;
    }
}

